/*jslint node: true */
// echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
'use strict';

const gulp = require('gulp');
const gulpIf = require('gulp-if');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const rimraf = require('rimraf');
const browserSync = require('browser-sync').create();

// NOTE: STYLES
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const mergemedia = require('gulp-merge-media-queries');
const cssclean = require('gulp-clean-css');
sass.compiler = require('node-sass');

// NOTE: HTML
const pug = require('gulp-pug');

// NOTE: IMAGE
const imagemin = require('gulp-imagemin');
const cache = require('gulp-cache');
// NOTE: SCRIPTS
const rigger = require('gulp-rigger');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');

const browserify = require('browserify');
const tslint = require("gulp-tslint");
const tsify = require("tsify");
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const gutil = require('gulp-util');

// const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev';
const isDev = false;
const isNotify = false;
// NOTE: path variables
const path = {
    nameProject: 'shulzbikes',
    build: {
        html: './frontend/web/build/',
        js: './frontend/web/build/js/',
        jsaddons: './frontend/web/build/js/addons/',
        css: './frontend/web/build/css/',
        img: './frontend/web/build/img/',
        fonts: './frontend/web/build/fonts/'
    },
    buildWeb: {
        bundle: './frontend/web/bundle/',
        fonts: './frontend/web/fonts/',
        img: './frontend/web/img/',
    },
    source: {
        pug: './frontend/web/source/pug/*.pug',
        js: './frontend/web/source/scripts/bundle.js',
        ts: './frontend/web/source/scripts/index.ts',
        jsaddons: './frontend/web/source/scripts/addons/*.js',
        sass: './frontend/web/source/sass/**/*.scss',
        img: './frontend/web/source/img/**/*.*',
        fonts: './frontend/web/source/fonts/**/*.*'
    },
    watch: {
        pug: './frontend/web/source/pug/**/*.pug',
        js: './frontend/web/source/scripts/**/*.js',
        ts: './frontend/web/source/scripts/**/*.ts',
        sass: './frontend/web/source/sass/**/*.scss',
        img: './frontend/web/source/img/**/*.*',
        fonts: './frontend/web/source/fonts/**/*.*'
    },
    clean: './frontend/web/build/**/*.*',
    cleanWeb: {
        bundle: './frontend/web/bundle/**/*.*',
        ignore: './frontend/web/**/.gitignore',
        img: './frontend/web/img/**/*.*',
        fonts: './frontend/web/fonts/**/*.*',
    }
};

// NOTE: server config

// TODO: CLEAN BUILD DIR
gulp.task('clean', (cb) => {
    const arr = [path.clean,path.cleanWeb.bundle, path.cleanWeb.img, path.cleanWeb.fonts ];
    arr.forEach(value => {
        return rimraf(value, {
            glob: {
                ignore: path.cleanWeb.ignore
            }
        }, cb);
    });
});

// TODO: WEBSERVER
gulp.task('webserver', () => {
    browserSync.init({
        server: {
            baseDir: path.build.html,
            directory: true
        },
        ui: { port: 8080 },
        // tunnel: path.nameProject,
        host: 'localhost',
        port: 7800,
        notify: false,
        logPrefix: 'webserver',
    });
    const reload = browserSync.reload;
    gulp.watch(path.watch.pug, gulp.series('pug:build'));
    gulp.watch(path.watch.sass, gulp.series('sass:build'));
    gulp.watch(path.watch.js, gulp.series('babel:build'));
    // gulp.watch(path.watch.ts, gulp.series('types:build')).on('change', reload);
    gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
    gulp.watch(path.watch.img, gulp.series('image:build'));
});


// TODO: COMPILE PUG
gulp.task('pug:build', () => {
    return gulp.src(path.source.pug)
        .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
        .pipe(pug({ pretty: true }))
        .pipe(plumber.stop())
        .pipe(gulpIf(isNotify, notify("COMPILE: <%= file.relative %>")))
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream());
});

// TODO: COMPILE SASS
gulp.task('sass:build', () => {
    return gulp.src(path.source.sass)
        .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
        .pipe(gulpIf(isDev, sourcemaps.init()))
        .pipe(sass.sync(gulpIf(!isDev, { outputStyle: 'compressed' })).on('error', sass.logError))
        // .pipe(mergemedia({ log: false }))
        .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
        .pipe(gulpIf(!isDev, cssclean()))
        .pipe(gulpIf(isDev, sourcemaps.write('./')))
        .pipe(plumber.stop())
        .pipe(gulpIf(isNotify, notify("COMPILE: <%= file.relative %>")))
        .pipe(gulpIf(isDev, gulp.dest(path.build.css)))
        .pipe(gulp.dest(path.buildWeb.bundle))
        .pipe(browserSync.stream());
});

// TODO: COMPILE JS
gulp.task('babel:build', () => {
    return gulp.src(path.source.js)
        .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
        .pipe(rigger())
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
        .pipe(gulpIf(isDev, sourcemaps.init()))
        .pipe(babel({ presets: ['@babel/env'] }))
        .pipe(gulpIf(!isDev, uglify()))
        .pipe(gulpIf(isDev, sourcemaps.write('./')))
        .pipe(plumber.stop())
        .pipe(gulpIf(isNotify, notify("COMPILE: <%= file.relative %>")))
        .pipe(gulpIf(isDev, gulp.dest(path.build.js)))
        .pipe(gulp.dest(path.buildWeb.bundle))
        .pipe(browserSync.stream());
});

// TODO: COMPILE TS 
// gulp.task('types:build', () => {
//     return browserify({
//         basedir: '.',
//         debug: true,
//         entries: [path.source.ts],
//         cache: {},
//         packageCache: {}
//     })
//     .plugin(tsify)
//     .transform("babelify")
//     .bundle()
//     .pipe(source('index.js'))
//     .pipe(buffer())
//     .pipe(gulpIf(isDev, sourcemaps.init({loadMaps: true})))
//     .pipe(gulpIf(!isDev,uglify()))
//     .pipe(gulpIf(isDev,sourcemaps.write('./')))
//     .pipe(gulp.dest(path.build.js));
// });

// TODO: COPY ADDINS 
gulp.task('jsaddons:build', () => {
    return gulp.src(path.source.jsaddons)
        .pipe(plumber({ handleError: function (err) { console.log(err); this.emit('end'); } }))
        .pipe(gulpIf(isDev, gulp.dest(path.build.jsaddons)))
        .pipe(gulp.dest(path.buildWeb.bundle))
        .pipe(browserSync.stream());
});

// TODO: COMPILE FONTS
gulp.task('fonts:build', () => {
    return gulp.src(path.source.fonts)
        .pipe(plumber({ handleError: function (err) { console.log(err); this.emit('end'); } }))
        .pipe(plumber.stop())
        .pipe(gulpIf(isDev, gulp.dest(path.build.fonts)))
        .pipe(gulp.dest(path.buildWeb.fonts))
        .pipe(browserSync.stream());
});

// TODO: COMPILE IMAGE
gulp.task('image:build', () => {
    return gulp.src(path.source.img)
        .pipe(plumber({ handleError: function (err) { console.log(err); this.emit('end'); } }))
        .pipe(gulpIf(!isDev, cache(
            imagemin({
                interlaced: true,
                progressive: true,
                optimizationLevel: 5,
                svgoPlugins: [{ removeViewBox: true }]
            })
        )
        ))
        .pipe(plumber.stop())
        .pipe(gulpIf(isDev, gulp.dest(path.build.img)))
        .pipe(gulp.dest(path.buildWeb.img))
        .pipe(browserSync.stream());
});


// TODO: RUN BUILD 
// gulp.task('build', gulp.series('clean', gulp.parallel('pug:build', 'sass:build', 'fonts:build', 'image:build', 'babel:build', 'types:build', 'jsaddons:build')));
gulp.task('build', gulp.series('clean', gulp.parallel('pug:build', 'sass:build', 'fonts:build', 'image:build', 'babel:build', 'jsaddons:build')));

// TODO: RUN DEV
gulp.task('default', gulp.series('build', 'webserver'));
gulp.task('prod', gulp.series('build'));