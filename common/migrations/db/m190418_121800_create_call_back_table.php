<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%call_back}}`.
 */
class m190418_121800_create_call_back_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%call_back}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'note' => $this->text(),
            'phone' => $this->string(150),
            'created_at' => $this->integer(),
            'status' => $this->smallInteger(1)->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%call_back}}');
    }
}
