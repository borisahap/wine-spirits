<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_course}}`.
 */
class m190425_095748_create_order_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_course}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'count' => $this->smallInteger(3)->defaultValue(1),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'note' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_course}}');
    }
}
