<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%hall}}`.
 */
class m190417_065223_create_hall_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%hall}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'sub_name' => $this->string(100),
            'description' => $this->text()->notNull(),
            'image' => $this->string(100)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%hall}}');
    }
}
