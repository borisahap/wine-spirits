<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%menu}}`.
 */
class m190408_135622_create_menu_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey(),
            'meta_title' => $this->string(512),
            'meta_description' => $this->text(),
            'image' => $this->string(),
            'banner_image' => $this->string(),
            'name' => $this->string(50),
            'description' => $this->text(),
            'slug' => $this->string(),
            'parent_id' => $this->integer()->defaultValue(0),
            'status' => $this->smallInteger(1)->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('id-parent_id','menu');
        $this->dropTable('{{%menu}}');

    }
}
