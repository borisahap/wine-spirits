<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%gallery}}`.
 */
class m190410_083746_create_gallery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%gallery}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
        $this->createTable('{{%img_gallery}}', [
            'id' => $this->primaryKey(),
            'gallery_id' => $this->integer(),
            'path' => $this->string()->notNull(),
            'base_url' => $this->string(),
            'type' => $this->string(),
            'size' => $this->integer(),
            'name' => $this->string(),
            'created_at' => $this->integer()
        ]);
        $this->addForeignKey('gallery_img', '{{%img_gallery}}', 'gallery_id', '{{%gallery}}', 'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('gallery_img', 'img_gallery');
        $this->dropTable('{{%gallery}}');
        $this->dropTable('{{%img_gallery}}');
    }
}
