<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%programm_courses}}`.
 */
class m190422_062847_create_programm_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%program_courses}}', [
            'id' => $this->primaryKey(),
            'course_id' =>$this->integer(),
            'date' => $this->integer(),
            'name' => $this->string(255),
            'description' => $this->text()
        ]);
        $this->addForeignKey('course_program_course', '{{%program_courses}}', 'course_id', '{{%courses}}', 'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('course_program_course','{{%program_courses}}');
        $this->dropTable('{{%programm_courses}}');
    }
}
