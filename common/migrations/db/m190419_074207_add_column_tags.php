<?php

use yii\db\Migration;

/**
 * Class m190419_074207_add_column_tags
 */
class m190419_074207_add_column_tags extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('article','tags',$this->string(512)->notNull());
        $this->addColumn('posters','tags',$this->string(512)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('article','tags');
        $this->dropColumn('posters','tags');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190419_074207_add_column_tags cannot be reverted.\n";

        return false;
    }
    */
}
