<?php

use yii\db\Migration;

/**
 * Class m190419_100922_add_column_meta
 */
class m190419_100922_add_column_meta extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('article','meta_title',$this->string(200));
        $this->addColumn('article','meta_description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('article','meta_title');
       $this->dropColumn('article','meta_description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190419_100922_add_column_meta cannot be reverted.\n";

        return false;
    }
    */
}
