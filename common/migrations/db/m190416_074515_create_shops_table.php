<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%shops}}`.
 */
class m190416_074515_create_shops_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%shops}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(300)->notNull(),
            'image' => $this->string(125)->notNull(),
            'phone' => $this->string(100)->notNull(),
            'city' => $this->string(120)->notNull(),
            'street' => $this->string(150)->notNull(),
            'time' => $this->text()->notNull(),
            'latitude' => $this->float(),
            'longitude' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shops}}');
    }
}
