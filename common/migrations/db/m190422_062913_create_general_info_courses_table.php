<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%general_info_courses}}`.
 */
class m190422_062913_create_general_info_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%general_info_courses}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(150)->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%general_info_courses}}');
    }
}
