<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_poster}}`.
 */
class m190429_095855_create_order_poster_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_poster}}', [
            'id' => $this->primaryKey(),
            'poster_id' => $this->integer(),
            'name' => $this->string(200)->notNull(),
            'phone' => $this->string(200)->notNull(),
            'email' => $this->string(150)->notNull(),
            'count' => $this->smallInteger(2)->notNull(),
            'totalPrice' => $this->integer()->notNull(),
            'note' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(0)
        ]);
        $this->addForeignKey('fk_poster_order', '{{%order_poster}}', 'poster_id', '{{%posters}}', 'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_poster_order', '{{%order_poster}}');
        $this->dropTable('{{%order_poster}}');
    }
}
