<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%posters}}`.
 */
class m190410_120604_create_posters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%posters}}', [
            'id' => $this->primaryKey(),
            'meta_title' => $this->string(250),
            'meta_description' => $this->text(),
            'name' => $this->string(250),
            'thumbnail_base_url' => $this->string(1024),
            'thumbnail_path' => $this->string(1024),
            'small_description' => $this->text(),
            'description' => $this->text(),
            'date' => $this->integer(),
            'time' => $this ->string(),
            'price' => $this->smallInteger(5),
            'place' => $this->string(),
            'phone' => $this->string(),
            'count_place' =>$this->smallInteger(5),
            'slug' => $this->string(150)->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(0)

        ]);

        $this->createTable('{{%program_poster}}',[
           'id' => $this->primaryKey(),
           'poster_id' => $this->integer(),
           'name' => $this->string(150)->notNull(),
           'to_name' => $this->string(200),
            'image' => $this->string(200),
            'description' => $this->text()->notNull()
        ]);
        $this->addForeignKey('poster_program_poster', '{{%program_poster}}', 'poster_id', '{{%posters}}', 'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('poster_program_poster','program_poster');
        $this->dropTable('{{%program-poster}}');
        $this->dropTable('{{%posters}}');
    }
}
