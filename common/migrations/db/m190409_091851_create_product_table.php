<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m190409_091851_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'cat_id' => $this->smallInteger(2)->notNull(),
            'name' => $this->text()->notNull(),
            'price' => $this->smallInteger(4)->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(0),
        ]);
        $this->createTable('{{%hots}}',[
            'id' => $this->primaryKey(),
            'cat_id' => $this->smallInteger(2)->notNull(),
            'name' => $this->string(150)->notNull(),
            'description' => $this->text(),
            'image' => $this->string(100)->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'date_start' => $this->integer()->notNull(),
            'date_finished' => $this->integer()
        ]);
//        $this->addForeignKey('product_cat','product','cat_id','menu','id','cascade','cascade');
//        $this->addForeignKey('hots_cat','hots','cat_id','menu','id','cascade','cascade');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->dropForeignKey('product_cat','product');
//        $this->dropForeignKey('hots_cat','hots');
        $this->dropTable('{{%product}}');
        $this->dropTable('{{%hots}}');
    }
}
