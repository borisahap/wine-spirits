<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%team}}`.
 */
class m190409_082955_create_team_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%team}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(130)->notNull(),
            'description' => $this->text()->notNull(),
            'position' => $this->string(100)->notNull(),
            'image' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%team}}');
    }
}
