<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%courses}}`.
 */
class m190422_062832_create_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%courses}}', [
            'id' => $this->primaryKey(),
            'meta_title' => $this->string(255),
            'meta_description' => $this->text(),
            'name' => $this->string(150)->notNull(),
            'image' => $this->string(150)->notNull(),
            'description' => $this->text()->notNull(),
            'date' => $this->integer()->notNull(),
            'result' => $this->text()->notNull(),
            'after_course' => $this->text()->notNull(),
            'trainig' => $this->text(),
            'exam' => $this->boolean(),
            'price' => $this->smallInteger(5),
            'count' => $this->smallInteger(3),
            'place' => $this->string(150),
            'phone' => $this->string(100),
            'time' => $this->string(50),
            'note' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'teacher_id'=> $this->integer()->notNull(),
            'slug' => $this->string(150)->notNull(),
            'views' => $this->integer()->defaultValue(0)
        ]);
        $this->createIndex('teacher_id','{{%courses}}','teacher_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('teacher_id','{{%courses}}');
        $this->dropTable('{{%courses}}');
    }
}
