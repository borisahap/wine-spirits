<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order}}`.
 */
class m190415_093412_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'date' => $this->integer()->notNull(),
            'time' => $this->string(),
            'phone' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'count' => $this->smallInteger(2)->notNull(),
            'note' => $this->text(),
            'my_note' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order}}');
    }
}
