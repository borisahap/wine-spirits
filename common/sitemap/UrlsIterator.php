<?php declare(strict_types=1);

namespace common\sitemap;

use common\models\Article;
use common\models\Courses;
use common\models\Menu;
use common\models\Page;
use common\models\Poster;
use Sitemaped\Element\Urlset\Url;

class UrlsIterator extends \AppendIterator
{
    /**
     * UrlsIterator constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->append($this->createPageLinksGenerator());
        $this->append($this->createArticleLinksGenerator());
        $this->append($this->createPosterLinksGenerator());
        $this->append($this->createCourseLinksGenerator());
        $this->append($this->createMenuLinksGenerator());
    }

    /**
     * @return \Generator
     */
    protected function createPageLinksGenerator()
    {
        $url = new Url(
            \yii\helpers\Url::toRoute(['/site/about'], true),
            (new \DateTime())->setTimestamp(time()),
            Url::CHANGEFREQ_MONTHLY, 0.6
        );
        yield $url;

        $url = new Url(
            \yii\helpers\Url::toRoute(['/site/service'], true),
            (new \DateTime())->setTimestamp(time()),
            Url::CHANGEFREQ_MONTHLY, 0.6
        );
        yield $url;

        $url = new Url(
            \yii\helpers\Url::toRoute(['/site/contact'], true),
            (new \DateTime())->setTimestamp(time()),
            Url::CHANGEFREQ_MONTHLY, 0.6
        );
        yield $url;

        $url = new Url(
            \yii\helpers\Url::to(['/site/shop'], true),
            (new \DateTime())->setTimestamp(time()),
            Url::CHANGEFREQ_MONTHLY, 0.6
        );
        yield $url;

    }

    /**
     * @return \Generator
     */
    protected function createArticleLinksGenerator(): \Generator
    {
        $models = Article::find()->published()->each(1000);
        foreach ($models as $model) {
            /** @var Article $model */
            $url = new Url(
                \yii\helpers\Url::to(['/article/view', 'slug' => $model->slug], true),
                (new \DateTime())->setTimestamp($model->updated_at),
                Url::CHANGEFREQ_WEEKLY, 0.6
            );
            yield $url;
        }
    }

    protected function createPosterLinksGenerator(): \Generator
    {
        $models = Poster::find()->where(['status' => Poster::STATUS_PUBLISHED])->each(1000);
        foreach ($models as $model) {
            /** @var Article $model */
            $url = new Url(
                \yii\helpers\Url::to(['/poster/view', 'slug' => $model->slug], true),
                (new \DateTime())->setTimestamp($model->date),
                Url::CHANGEFREQ_WEEKLY, 0.8
            );
            yield $url;
        }
    }

    protected function createCourseLinksGenerator(): \Generator
    {
        $models = Courses::find()->where(['status' => Courses::STATUS_PUBLISHED])->each(1000);
        foreach ($models as $model) {
            /** @var Article $model */
            $url = new Url(
                \yii\helpers\Url::to(['/course/view', 'slug' => $model->slug], true),
                (new \DateTime())->setTimestamp($model->date),
                Url::CHANGEFREQ_WEEKLY, 0.8
            );
            yield $url;
        }
    }

    protected function createMenuLinksGenerator(): \Generator
    {
        $models = Menu::find()->where(['status' => Menu::STATUS_PUBLISHED])
            ->andWhere(['parent_id' => null])
            ->each(1000);
        foreach ($models as $model) {
            /** @var Article $model */
            $url = new Url(
                \yii\helpers\Url::to(['/menu/view', 'menu' => $model->slug], true),
                (new \DateTime())->setTimestamp(time()),
                Url::CHANGEFREQ_WEEKLY, 0.8
            );
            yield $url;
        }
    }
}
