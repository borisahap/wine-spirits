<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "posters".
 *
 * @property int $id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $name
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property string $small_description
 * @property string $description
 * @property int $date
 * @property string $time
 * @property int $price
 * @property string $place
 * @property string $phone
 * @property int $status
 *
 * @property ProgramPoster[] $programPosters
 */
class Poster extends \yii\db\ActiveRecord
{
    public $thumbnail;
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;
    const STATUS_DONE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posters';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'immutable' => true,
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meta_description', 'slug', 'small_description', 'description'], 'string'],
            [['status', 'count_place'], 'integer'],
            [['meta_title', 'name'], 'string', 'max' => 250],
            [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['time', 'place', 'phone'], 'string', 'max' => 255],
            [['name', 'date', 'status','tags', 'count_place', 'description'], 'required'],
            [['price'], 'integer'],
            [['thumbnail'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'name' => 'Название мероприятия',
            'thumbnail' => 'Изображение мероприятия',
            'small_description' => 'Тезисное описание',
            'description' => 'Полное описание',
            'date' => 'Дата проведения',
            'tags' => 'Тэги',
            'time' => 'Время начала мероприятия',
            'price' => 'Цена, BYN',
            'place' => 'Место проведения',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'count_place' => 'Количество мест'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramPosters()
    {
        return $this->hasMany(ProgramPoster::className(), ['poster_id' => 'id']);
    }

    public static function statuses()
    {
        return [
            self::STATUS_DRAFT => Yii::t('common', 'Новое'),
            self::STATUS_PUBLISHED => Yii::t('common', 'Опубликован'),
            self::STATUS_DONE => Yii::t('common', 'Завершено'),
        ];
    }

    public function getImg()
    {
        return $this->thumbnail_base_url . str_replace('\\', '/', $this->thumbnail_path);
    }

}
