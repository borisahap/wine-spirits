<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property int $id
 * @property string $name
 *
 * @property ImgGallery[] $imgGalleries
 */
class Gallery extends \yii\db\ActiveRecord
{
    public $attachments;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gallery';
    }

    public function behaviors()
    {
        return [

            [
                'class' => UploadBehavior::class,
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'imgGalleries',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute' => 'order',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'name',
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            ['name', 'required'],
            ['name', 'unique'],
            [['attachments'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название галлереи',
            'attachments' => 'Фотографии',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImgGalleries()
    {
        return $this->hasMany(ImgGallery::className(), ['gallery_id' => 'id']);
    }

    public function getImg()
    {
        return $this->base_url.$this->path;
    }
}
