<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "program_courses".
 *
 * @property int $id
 * @property int $course_id
 * @property int $date
 * @property string $name
 * @property string $description
 *
 * @property Courses $course
 */
class ProgramCourse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'program_courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date','name','description'],'required'],
            [['course_id'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Курс',
            'date' => 'Дата',
            'name' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }
}
