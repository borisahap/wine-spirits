<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "hots".
 *
 * @property int $id
 * @property int $cat_id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property int $status
 * @property string $date_start
 * @property string $date_finished
 */
class Hot extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hots';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'name', 'date_start'], 'required'],
            [['cat_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 150],
            [['image'], 'string', 'max' => 100],
            [['date_start', 'date_finished'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Категория',
            'name' => 'Название',
            'description' => 'Описание',
            'image' => 'Изображение',
            'status' => 'Статус',
            'date_start' => 'Дата начала акции',
            'date_finished' => 'Дата конца акции',
        ];
    }

    public function saveImage()
    {
        $image = UploadedFile::getInstance($this, 'image');
        if (!empty($image)) {
            FileHelper::createDirectory(Yii::getAlias('@storage/web/menu-image/product-image'), 0775, true);
            $name = Yii::$app->security->generateRandomString();
            $path = Yii::getAlias('@storage/web/menu-image/product-image/' . $name . '.' . $image->extension);
            $image->saveAs($path);
            $this->image = $name . '.' . $image->extension;
        }
    }

    public static function statuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Не активно'),
            self::STATUS_PUBLISHED => Yii::t('common', 'Активно'),
        ];
    }

    public function getImg()
    {
        return Yii::getAlias('@host_storage/menu-image/product-image/') . $this->image;
    }

    public function getCategory()
    {
        return $this->hasOne(Menu::class, ['id' => 'cat_id']);
    }
}
