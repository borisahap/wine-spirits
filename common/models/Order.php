<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $date
 * @property string $time
 * @property string $phone
 * @property string $name
 * @property int $count
 * @property string $note
 * @property int $status
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_CANCEL = 1;
    const STATUS_NOTCALL = 2;
    const STATUS_DONE = 3;

    const VALID_FORM = [
       'date', 'time', 'name','phone'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'phone', 'name', 'count', 'time'], 'required', 'message' => 'Поле обязательно'],
            [['count', 'status'], 'integer'],
            [['note'], 'string'],
            [['time', 'name'], 'string', 'max' => 255],
            ['phone', 'match', 'pattern' => '%^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$%', 'message' => 'Укажите настоящий номер телефона'],
            [['my_note'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'На какую дату',
            'time' => 'На какое время',
            'phone' => 'Телефон',
            'name' => 'Имя',
            'count' => 'Количество персон',
            'note' => 'Примечание',
            'status' => 'Статус',
            'my_note' => 'Примечание W&SS'
        ];
    }

    public static function statuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Новое'),
            self::STATUS_CANCEL => Yii::t('common', 'Отменено'),
            self::STATUS_NOTCALL => Yii::t('common', 'Не удалось дозвониться'),
            self::STATUS_DONE => Yii::t('common', 'Забронировано'),

        ];
    }
}
