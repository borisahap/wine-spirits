<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $name
 * @property string $description
 * @property int $parent_id
 * @property int $status
 *
 * @property Menu $parent
 * @property Menu[] $menus
 */
class Menu extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_PUBLISHED = 1;

    public function behaviors()
    {
        return [

            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'immutable' => true,
            ],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meta_description', 'description'], 'string'],
            [['parent_id', 'status'], 'integer'],
            ['name', 'required', 'message' => 'Название не может быть пустым'],
            [['meta_title'], 'string', 'max' => 512],
            [['image', 'banner_image'], 'safe'],
            [['image','slug'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_title' => 'Seo Title',
            'meta_description' => 'Seo Description',
            'name' => 'Название',
            'image' => 'Изображение для категории',
            'banner_image' => 'Изображение для баннера',
            'description' => 'Описание',
            'parent_id' => 'Родительская категория',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Menu::className(), ['id' => 'parent_id']);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function statuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Не активно'),
            self::STATUS_PUBLISHED => Yii::t('common', 'Активно'),
        ];
    }

    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['parent_id' => 'id']);
    }

    public function saveImage()
    {
        $image = UploadedFile::getInstance($this, 'image');
        $banner_image = UploadedFile::getInstance($this, 'banner_image');

        if (!empty($image)) {
            FileHelper::createDirectory(Yii::getAlias('@storage/web/menu-image/'), 0775, true);
            $name = Yii::$app->security->generateRandomString();
            $path = Yii::getAlias('@storage/web/menu-image/' . $name . '.' . $image->extension);
            $image->saveAs($path);
            $this->image = $name . '.' . $image->extension;
        }
        if (!empty($banner_image)) {
            FileHelper::createDirectory(Yii::getAlias('@storage/web/menu-image/banner/'), 0775, true);
            $name = Yii::$app->security->generateRandomString();
            $path = Yii::getAlias('@storage/web/menu-image/banner/' . $name . '.' . $banner_image->extension);
            $banner_image->saveAs($path);
            $this->banner_image = $name . '.' . $banner_image->extension;
        }
    }

    public function getImg()
    {
        return Yii::getAlias('@host_storage/menu-image/') . $this->image;
    }

    public function getBanner()
    {
        return Yii::getAlias('@host_storage/menu-image/banner/') . $this->banner_image;
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['cat_id' => 'id']);

    }
}
