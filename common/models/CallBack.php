<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "call_back".
 *
 * @property int $id
 * @property string $name
 * @property string $note
 * @property string $phone
 * @property int $created_at
 * @property int $status
 */
class CallBack extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_DONE = 1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false
            ]

        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'call_back';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['note'], 'string'],
            [['created_at', 'status'], 'integer'],
            [['name', 'phone'], 'string', 'max' => 150],
            ['phone', 'match', 'pattern' => '%^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$%', 'message' => 'Укажите настоящий номер телефона'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'note' => 'Комментарий',
            'phone' => 'Телефон',
            'created_at' => 'Написано',
            'status' => 'Статус',
        ];
    }
    public static function statuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Новое'),
            self::STATUS_DONE => Yii::t('common', 'Просмотренное'),
        ];
    }
}
