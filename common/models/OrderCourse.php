<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_course".
 *
 * @property int $id
 * @property int $course_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property int $count
 * @property int $status
 */
class OrderCourse extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_DONE = 1;
    const STATUS_CANCEL = 2;
    const STATUS_NOTCALL = 4;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'count', 'status'], 'integer'],
            [['name', 'email', 'phone', 'count'], 'required'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            ['phone', 'match', 'pattern' => '%^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$%', 'message' => 'Укажите настоящий номер телефона'],
            ['email', 'email'],
            ['note','safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'На курс',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'count' => 'Количество человек',
            'status' => 'Status',
            'note' => 'Мое примечание'
        ];
    }

    public function getCourse()
    {
        return $this->hasOne(Courses::class, ['id' => 'course_id']);
    }

    public static function statuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Новое'),
            self::STATUS_DONE => Yii::t('common', 'Обработан'),
            self::STATUS_CANCEL => Yii::t('common', 'Отменен'),
            self::STATUS_NOTCALL => Yii::t('common', 'Не удалось связаться'),
        ];
    }
}
