<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "teachers".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $description
 *
 * @property Courses $id0
 */
class Teachers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teachers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'image' => 'Изображение',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasOne(Courses::className(), ['teacher_id' => 'id']);
    }

    public function getImg()
    {
        return Yii::getAlias('@host_storage/course-image/teacher-image/') . $this->image;
    }

    public function saveImage()
    {
        $image = UploadedFile::getInstance($this, 'image');
        if (!empty($image)) {
            FileHelper::createDirectory(Yii::getAlias('@storage/web/course-image/teacher-image'), 0775, true);
            $name = Yii::$app->security->generateRandomString();
            $path = Yii::getAlias('@storage/web/course-image/teacher-image/' . $name . '.' . $image->extension);
            $image->saveAs($path);
            $this->image = $name . '.' . $image->extension;
        }
    }
}
