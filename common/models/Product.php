<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $cat_id
 * @property string $name
 * @property int $price
 * @property int $status
 */
class Product extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_PUBLISHED = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'name', 'price'], 'required','message'=>'Поле  не может быть пустым'],
            [['cat_id', 'price', 'status'], 'integer'],
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function statuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Не активно'),
            self::STATUS_PUBLISHED => Yii::t('common', 'Активно'),
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Категория',
            'name' => 'Название',
            'price' => 'Цена, BYN',
            'status' => 'Статус',
        ];
    }
    public function getCategory()
    {
        return $this->hasOne(Menu::class,['id'=>'cat_id']);
    }
}
