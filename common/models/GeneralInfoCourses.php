<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "general_info_courses".
 *
 * @property int $id
 * @property string $image
 * @property string $name
 * @property string $description
 */
class GeneralInfoCourses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'general_info_courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'name', 'description'], 'required'],
            [['description'], 'string'],
            [['image'], 'string', 'max' => 150],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'name' => 'Название',
            'description' => 'Описание',
        ];
    }

    public function getImg()
    {
        return Yii::getAlias('@host_storage/course-image/general-image/') . $this->image;
    }

    public function saveImage()
    {
        $image = UploadedFile::getInstance($this, 'image');
        if (!empty($image)) {
            FileHelper::createDirectory(Yii::getAlias('@storage/web/course-image/general-image'), 0775, true);
            $name = Yii::$app->security->generateRandomString();
            $path = Yii::getAlias('@storage/web/course-image/general-image/' . $name . '.' . $image->extension);
            $image->saveAs($path);
            $this->image = $name . '.' . $image->extension;
        }
    }
}
