<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_poster".
 *
 * @property int $id
 * @property int $poster_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property int $count
 * @property int $status
 *
 * @property Posters $poster
 */
class OrderPoster extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_DONE = 1;
    const STATUS_CANCEL = 2;
    const STATUS_NOTCALL = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_poster';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['poster_id', 'count', 'status', 'totalPrice'], 'integer'],
            [['name', 'email', 'phone', 'count','poster_id'], 'required'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            ['phone', 'match', 'pattern' => '%^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$%', 'message' => 'Укажите настоящий номер телефона'],
            ['email', 'email'],
            ['note', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'poster_id' => 'Афиша',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'count' => 'Количество персон',
            'status' => 'Статус',
            'note' => 'Примечание',
            'totalPrice' => 'Итоговая стоимость, BYN'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoster()
    {
        return $this->hasOne(Poster::className(), ['id' => 'poster_id']);
    }

    public static function statuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Новое'),
            self::STATUS_DONE => Yii::t('common', 'Обработан'),
            self::STATUS_CANCEL => Yii::t('common', 'Отменен'),
            self::STATUS_NOTCALL => Yii::t('common', 'Не удалось связаться'),
        ];
    }
}
