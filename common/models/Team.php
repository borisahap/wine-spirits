<?php

namespace common\models;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use Yii;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $position
 * @property string $image
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'position', 'image'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 130],
            [['position'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Ф.И.О',
            'description' => 'Описание',
            'position' => 'Должность',
            'image' => 'Фото',
        ];
    }

    public function saveImage()
    {
        $image = UploadedFile::getInstance($this, 'image');
        if (!empty($image)) {
            FileHelper::createDirectory(Yii::getAlias('@storage/web/team-image/'), 0775, true);
            $name = Yii::$app->security->generateRandomString();
            $path = Yii::getAlias('@storage/web/team-image/' . $name . '.' . $image->extension);
            $image->saveAs($path);
            $this->image = $name . '.' . $image->extension;
        }
    }

    public function getImg()
    {
        return Yii::getAlias('@host_storage/team-image/') . $this->image;
    }
}
