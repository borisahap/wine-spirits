<?php

namespace common\models;

use Yandex\Geo\Api;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "shops".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $phone
 * @property string $city
 * @property string $street
 * @property string $time
 * @property string $latitude
 * @property string $longitude
 */
class Shop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shops';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image', 'phone', 'city', 'street', 'time'], 'required'],
            [['time'], 'string'],
            [['name'], 'string', 'max' => 300],
            [['image'], 'string', 'max' => 125],
            [['phone'], 'string', 'max' => 100],
            [['city'], 'string', 'max' => 120],
            [['street'], 'string', 'max' => 150],
            [['latitude', 'longitude'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Изображение',
            'phone' => 'Телефоны',
            'city' => 'Город',
            'street' => 'Улица',
            'time' => 'Время работы',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }

    public function saveImage()
    {
        $image = UploadedFile::getInstance($this, 'image');
        if (!empty($image)) {
            FileHelper::createDirectory(Yii::getAlias('@storage/web/shop-image'), 0775, true);
            $name = Yii::$app->security->generateRandomString();
            $path = Yii::getAlias('@storage/web/shop-image/' . $name . '.' . $image->extension);
            $image->saveAs($path);
            $this->image = $name . '.' . $image->extension;
        }
    }

    public static function getGeoPoint(string $city, string $street, string $geo)
    {
        $api = new Api();
        $api->setQuery($city . ' ' . $street);
        $api->setLimit(1)// кол-во результатов
        ->setLang(Api::LANG_RU)// локаль ответа
        ->load();

        $response = $api->getResponse();
        $collection = $response->getList();
        $data = 0;
        foreach ($collection as $item) {
            $data = $item->getData()[$geo];
        }
        return $data;
    }

    public function getImg()
    {
        return Yii::getAlias('@host_storage/shop-image/') . $this->image;
    }
}
