<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $name
 * @property string $image
 * @property string $description
 * @property string $result
 * @property string $after_course
 * @property string $trainig
 * @property int $exam
 * @property int $price
 * @property int $count
 * @property string $place
 * @property string $phone
 * @property string $time
 * @property string $note
 * @property int $status
 * @property int $teacher_id
 *
 * @property ProgramCourses[] $programCourses
 * @property Teachers $teachers
 */
class Courses extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_PUBLISHED = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    public function behaviors()
    {
        return [

            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'immutable' => true,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meta_description', 'description', 'result', 'after_course', 'trainig', 'note', 'image'], 'string'],
            [['name', 'description', 'result', 'after_course', 'teacher_id', 'date', 'slug'], 'required'],
            [['exam', 'price', 'count', 'status', 'teacher_id', 'views'], 'integer'],
            [['meta_title'], 'string', 'max' => 255],
            [['name', 'image', 'place'], 'string', 'max' => 150],
            [['phone'], 'string', 'max' => 100],
            [['time'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'name' => 'Название курса',
            'image' => 'Изображение',
            'description' => 'Описание курса',
            'result' => 'На курсе вы узнаете ...',
            'after_course' => 'После курсов вы сможете ...',
            'trainig' => 'Как происходит обучение',
            'exam' => 'Экзамен',
            'price' => 'Цена, BYN',
            'count' => 'Количество занятий',
            'place' => 'Место проведения',
            'phone' => 'Телефон',
            'date' => 'Дата начала',
            'time' => 'Время',
            'note' => 'Примечание',
            'status' => 'Статус',
            'teacher_id' => 'Преподаватель',
            'views' => 'Просмотры'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramCourses()
    {
        return $this->hasMany(ProgramCourse::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teachers::className(), ['id' => 'teacher_id']);
    }

    public function getImg()
    {
        return Yii::getAlias('@host_storage/course-image/courses-image/') . $this->image;
    }

    public function saveImage()
    {
        $image = UploadedFile::getInstance($this, 'image');
        if (!empty($image)) {
            FileHelper::createDirectory(Yii::getAlias('@storage/web/course-image/courses-image'), 0775, true);
            $name = Yii::$app->security->generateRandomString();
            $path = Yii::getAlias('@storage/web/course-image/courses-image/' . $name . '.' . $image->extension);
            $image->saveAs($path);
            $this->image = $name . '.' . $image->extension;
        }
    }

    public static function statuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Новый'),
            self::STATUS_PUBLISHED => Yii::t('common', 'Опубликован'),
        ];
    }
}
