<?php
/**
 * Created by PhpStorm.
 * User: JuniorPHP
 * Date: 15.04.2019
 * Time: 10:09
 */

namespace common\widgets;


use yii\jui\Widget;

class MainMenuWidget extends Widget
{
    public function run()
    {
        return $this->render('menuWidget');
    }
}