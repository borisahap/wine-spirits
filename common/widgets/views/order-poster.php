<?php

?>
<!-- modal-->
<div class="ui_modal modal fade" id="reservationTable<?=$poster->id?>" role="dialog">
    <div class="modal-dialog ui_modal--large">
        <form class="modal-content" id="ajax_form" action="<?=\yii\helpers\Url::toRoute(['/poster/order']) ?>" method="post">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" >
            <input type="hidden" name="OrderPoster[poster_id]" value="<?= $poster->id?>">
            <div class="modal-header ui_modal__title">
                <div class="ui_modal--title">Форма бронирования</div>
                <button class="ui_modal--close ui_button --transparent" type="button" data-dismiss="modal" aria-label="Close"><i class="icon-cross-out"></i></button>
            </div>
            <div class="modal-body ui_modal__reserv">
                <div class="ui_modal--success" style="display: none"> Ваша заявка принята, ожидайте, с вами свяжутся.</div>
                <div class="ui_form-group ui_modal__reserv--title">
                    <div class="ui_form-input">
                        <label class="_label" for="id4<?=$poster->id?>">
                            Мероприятие</label>
                        <input class="_input" id="id4<?=$poster->id?>" type="text" name="" value="<?= $poster->name?>" disabled>
                        <div class="_error"></div>
                    </div>
                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id40<?=$poster->id?>">
                            Дата</label>
                        <input class="_input" id="id40<?=$poster->id?>" type="text" name="" value="<?= Yii::$app->formatter->asDate($poster->date,'dd.MM.yyyy') ?>" disabled>
                        <div class="_error"></div>
                    </div>
                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id5<?=$poster->id?>">
                            Имя<span>*</span></label>
                        <input class="_input" id="id5<?=$poster->id?>" type="text" name="OrderPoster[name]">
                        <div class="_error"></div>
                    </div>
                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id50<?=$poster->id?>">
                            Телефон<span>*</span></label>
                        <input class="_input" id="id50<?=$poster->id?>" type="text" name="OrderPoster[phone]">
                        <div class="_error"></div>
                    </div>
                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id500<?=$poster->id?>">
                            E-mail<span>*</span></label>
                        <input class="_input" id="id500<?=$poster->id?>" type="text" name="OrderPoster[email]">
                        <div class="_error"></div>
                    </div>
                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id6<?=$poster->id?>">
                            Количество персон<span>*</span></label>
                        <input class="_input" id="id6<?=$poster->id?>" type="number" data-price-input name="OrderPoster[count]" min="1" max="30" value="1">
                        <div class="_error"></div>
                    </div>
                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id60<?=$poster->id?>">
                            Стоимость, BYN</label>
                        <input class="_input c_reserv--price" id="id60<?=$poster->id?>" type="text" data-price-value="<?=$poster->price?>" value="<?=$poster->price?>" name="OrderPoster[totalPrice]" disabled>
                        <div class="_error"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer ui_modal__footer">
                <button class="ui_modal--submit ui_button --default" type="button">забронировать</button>
                <div class="ui_modal--message">В случае отмены бронирования не менее чем за 48 часов до времени бронирования, осуществляется возврат денежных средств. </div>
            </div>
        </form>
    </div>
</div>
<!-- END modal-->
<?php $orderPoster = <<< JS
    $('.ui_modal--submit').click(function(e) {
        e.preventDefault();
      $.ajax({
      url: '/poster/order',
      type: "post",
      data: $("#ajax_form").serialize(),
      success: function(res) {
          console.log(res);
        if (res != true){

            $.each(res, function(key,val) {
                $('input[name="OrderPoster['+ key +']"]').next().html(val);
            });
        } else {
            $('._error').html('');
            $('.modal-body').find('.ui_form-group').hide();
            $('.modal-footer').hide();
            $('.ui_modal--success').show();
            $('.ui_modal--title').text('Успех!');
        }
      },
      error: function(array) {
        console.log(array); 
      }
      });
    })
JS;
$this->registerJs($orderPoster);
?>