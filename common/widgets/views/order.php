<?php
use yii\helpers\Url;
?>
<?php if (Yii::$app->session->getFlash('success') != null ) : ?>
    <div id="order" class="s_i-reservation__message"><?=Yii::$app->session->getFlash('success')?></div>
<?php else: ?>
<form class="s_i-reservation__content" method="post" action="<?= Url::toRoute(['/site'])?>">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" >
    <div id="order" class="ui_form-group s_i-reservation--datepicker">
        <div class="ui_form-input">
            <div class="_label">Дата<span>*</span></div>
            <label class="_icon-field icon-calendar" for="id3" data-datepicker-toggle>
                <input class="_input" id="id3" type="text" value="<?= Yii::$app->session->getFlash('oldData') != null ? Yii::$app->formatter->asDate(Yii::$app->session->getFlash('oldData')['date'],'dd.MM.yyyy') : '' ?>" name="Order[date]">
            </label>
            <div class="c_datepicker">
                <div class="c_datepicker--title">Календарь бронирования</div>
                <button class="c_datepicker--close" type="button"><i class="icon-cross-out"></i></button>
                <div class="c_datepicker-wrap"> </div>
            </div>
            <div class="_error" style="color: red;"><p><?= Yii::$app->session->getFlash('errors') != null && array_key_exists ('date',Yii::$app->session->getFlash('errors')) ? Yii::$app->session->getFlash('errors')['date'][0] : ''?></p></div>
        </div>
    </div>
    <div class="ui_form-group">
        <div class="ui_form-input">
            <label class="_label" for="id4">
                Время<span>*</span></label>
            <input class="_input" id="id4" type="time" value="<?= Yii::$app->session->getFlash('oldData') != null ? Yii::$app->session->getFlash('oldData')['time'] : '' ?>" name="Order[time]">
            <div class="_error" style="color: red;"><p><?= Yii::$app->session->getFlash('errors') != null && array_key_exists ('time',Yii::$app->session->getFlash('errors')) ? Yii::$app->session->getFlash('errors')['time'][0] : ''?></p></div>
        </div>

    </div>
    <div class="ui_form-group">
        <div class="ui_form-input">
            <label class="_label" for="id5">
                Телефон<span>*</span></label>
            <input class="_input" id="id5" type="text" value="<?= Yii::$app->session->getFlash('oldData') != null ? Yii::$app->session->getFlash('oldData')['phone'] : '' ?>" name="Order[phone]">
            <div class="_error" style="color: red;"><p><?= Yii::$app->session->getFlash('errors') != null && array_key_exists ('phone',Yii::$app->session->getFlash('errors')) ? Yii::$app->session->getFlash('errors')['phone'][0] : ''?></p></div>

        </div>
    </div>
    <div class="ui_form-group">
        <div class="ui_form-input">
            <label class="_label" for="id5">
                Имя<span>*</span></label>
            <input class="_input" id="id5" type="text" value="<?= Yii::$app->session->getFlash('oldData') != null ? Yii::$app->session->getFlash('oldData')['name'] : '' ?>" name="Order[name]">
            <div class="_error" style="color: red;"><p><?= Yii::$app->session->getFlash('errors') != null && array_key_exists ('name',Yii::$app->session->getFlash('errors')) ? Yii::$app->session->getFlash('errors')['name'][0] : ''?></p></div>

        </div>
    </div>
    <div class="ui_form-group s_i-reservation--person">
        <div class="ui_form-input">
            <label class="_label" for="id6">
                Количество персон<span>*</span></label>
            <input class="_input" id="id6" type="number" min="0" step="1" value="<?= Yii::$app->session->getFlash('oldData') != null ? Yii::$app->session->getFlash('oldData')['date'] : '' ?>" name="Order[count]">
            <div class="_error" style="color: red;"><p><?= Yii::$app->session->getFlash('errors') != null && array_key_exists ('count',Yii::$app->session->getFlash('errors')) ? Yii::$app->session->getFlash('errors')['count'][0] : ''?></p></div>

        </div>
    </div>
    <div class="ui_form-group s_i-reservation--message">
        <div class="ui_form-input">
            <label class="_label" for="id7">Комментарии</label>
            <textarea class="_textarea" id="id7" name="Order[note]" rows="4"><?= Yii::$app->session->getFlash('oldData') != null ? Yii::$app->session->getFlash('oldData')['note'] : '' ?></textarea>
        </div>
    </div>
    <div class="ui_form-group s_i-reservation--submit">
        <button class="ui_button --default" type="submit">забронировать</button>
    </div>
</form>
<?php endif; ?>