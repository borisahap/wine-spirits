<?php
use yii\helpers\Url;
use common\widgets\OrderPosterWidget;
?>
<?php if ($main != null) : ?>
<div class="s_p-promo-card">
    <div class="s_p-promo-card--date"><?= Yii::$app->formatter->asDate($main->date,'dd.MM.yyyy')?></div>
    <div class="s_p-promo-card__tags">
        <?php foreach (explode(' ' ,$main->tags) as $tag) :?>
       <a href="#!"><?= $tag?></a>
        <?php endforeach; ?>
    </div><a class="s_p-promo-card--title" href="<?=Url::toRoute(['/poster/view','slug'=> $main->slug])?>"><?=$main->name?></a>
    <div class="s_p-promo-card__desc">
        <p><?= $main->description ?></p>
    </div>
    <div class="s_p-promo-card--price"><?=$main->price?> byn</div>
    <div class="s_p-promo-card__bottom"><button class="s_p-promo-card--button ui_button --outline --light" data-toggle="modal" data-target="#reservationTable<?=$main->id?>">забронировать </button>
        <div class="s_p-promo-card--logo"><img src="/img/logo.svg" alt="<?=$main->name?>" title="<?=$main->name?>"></div>
    </div>
</div>
<?php echo OrderPosterWidget::widget(['poster_id'=>$main->id]) ?>
<?php endif; ?>