<?php
use yii\helpers\Url;
?>
<div class="line-container--wrap"></div>
<div class="s_header__social"><a target="_blank" href="<?= Yii::$app->keyStorage->get('tripadvisor')?>">tripadvisor</a><a target="_blank" href="<?=Yii::$app->keyStorage->get('fb')?>">facebook</a><a target="_blank" href="<?=Yii::$app->keyStorage->get('insta')?>">instagram</a></div>
<div class="s_header"><a class="s_header--logo" href="/"><img src="/img/logo.svg"></a>
    <button class="s_header__nav--menu ui_button --transparent" type="button"> <i class="icon-menu"></i></button>
    <div class="s_header__nav">
        <div class="s_header__nav-wrap container"><a class="s_header__nav--logo" href="/"><img src="/img/logo.svg"></a>
            <button class="s_header__nav--close ui_button --invert" type="button"> <i class="icon-cross-out"></i></button>
            <div class="s_header__nav--social"> <a target="_blank" href="<?= Yii::$app->keyStorage->get('tripadvisor')?>">tripadvisor</a><a target="_blank" href="<?=Yii::$app->keyStorage->get('fb')?>">facebook</a><a target="_blank" href="<?=Yii::$app->keyStorage->get('insta')?>">instagram</a></div>
            <ul class="s_header__nav-list">
                <li class="s_header__nav-list--item"> <a class="s_header__nav-list--link <?=Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'about'  ? '--current' : '' ?>" href="<?=Url::toRoute(['/site/about'])?>">О нас</a></li>
                <li class="s_header__nav-list--item"> <a class="s_header__nav-list--link <?=Yii::$app->controller->id === 'poster' ? '--current' : '' ?>" href="<?=Url::toRoute(['/poster/index','month'=>date('m')])?>">Афиша</a></li>
                <li class="s_header__nav-list--item"> <a class="s_header__nav-list--link <?=Yii::$app->controller->id === 'course' ? '--current' : '' ?>" href="<?= Url::toRoute(['/course/index'])?>">W&S Academy</a></li>
                <li class="s_header__nav-list--item"> <a class="s_header__nav-list--link <?=Yii::$app->controller->id === 'menu' ? '--current' : '' ?>" href="<?=Url::toRoute(['/menu/index'])?>">Меню</a></li>
                <li class="s_header__nav-list--item"> <a class="s_header__nav-list--link <?=Yii::$app->controller->id === 'article' ? '--current' : '' ?>" href="<?=Url::toRoute(['/article/index'])?>">W&S News</a></li>
                <li class="s_header__nav-list--item"> <a class="s_header__nav-list--link <?=Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'service'  ? '--current' : '' ?>" href="<?=Url::toRoute(['/site/service'])?>">Услуги</a></li>
                <li class="s_header__nav-list--item"> <a class="s_header__nav-list--link <?=Yii::$app->controller->id === 'shop'  ? '--current' : '' ?>" href="<?=Url::toRoute(['/shop/index'])?>">W&S Shop</a></li>
                <li class="s_header__nav-list--item"> <a class="s_header__nav-list--link <?=Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'contact'  ? '--current' : '' ?>" href="<?=Url::toRoute(['/site/contact'])?>">Контакты</a></li>
            </ul>
        </div>
    </div>
</div>