<?php
/**
 * Created by PhpStorm.
 * User: JuniorPHP
 * Date: 15.04.2019
 * Time: 14:55
 */

namespace common\widgets;


use yii\jui\Widget;

class OrderWidget extends Widget
{
    public function run()
    {
        return $this->render('order');
    }
}