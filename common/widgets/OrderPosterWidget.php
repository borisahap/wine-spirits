<?php
/**
 * Created by PhpStorm.
 * User: JuniorPHP
 * Date: 25.04.2019
 * Time: 16:22
 */

namespace common\widgets;


use common\models\Poster;
use yii\jui\Widget;

class OrderPosterWidget extends Widget
{
    public $poster_id;

    public function run()
    {
        $poster = Poster::findOne($this->poster_id);
        return $this->render('order-poster',[
            'poster' => $poster
        ]);
    }

}