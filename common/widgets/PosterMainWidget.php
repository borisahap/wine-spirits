<?php


namespace common\widgets;


use yii\jui\Widget;
use common\models\Poster;

class PosterMainWidget extends Widget
{

    public function run()
    {
        $main = Poster::find()
            ->where(['status' => Poster::STATUS_PUBLISHED])
            ->andWhere(['>=', 'date', time()])
            ->orderBy(['date' => SORT_ASC])
            ->one() or null;
        return $this->render('mainPoster', [
            'main' => $main
        ]);
    }
}