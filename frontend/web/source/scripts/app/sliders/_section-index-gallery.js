$(() => {
   const sliderItem = $('.s_i-gallery-slider--item');
   const sliderImageBox = $('.s_i-gallery__imagebox img');
   const slider = $('.s_i-gallery-slider');
   const sliderNavItem = $('.s_i-gallery-slider-nav--item');

   const _sliderSlideNext = numSlide => {
       sliderNavItem.removeClass('--current');
       $(`.s_i-gallery-slider-nav--item[data-gallery-slide='${numSlide}']`).addClass('--current');
       slider.slick('slickGoTo', numSlide);
   };

   const _setImageBoxSrc = img => sliderImageBox.attr('src', img);

   slider.slick({
       dots: false,
       infinite: true,
       speed: 500,
       slidesToShow: 1,
       slidesToScroll: 1,
       fade: true,
       swipe: true,
       prevArrow: '.s_i-gallery-slider--prev',
       nextArrow: '.s_i-gallery-slider--next',
       adaptiveHeight: true,
       responsive: [
           {
               breakpoint: BREAKPOINT_LG_MIN,
               settings: {
                   swipe: false
               }
           }
       ]
   }).on('afterChange', function(slick, currentSlide) {
       _sliderSlideNext(currentSlide.currentSlide);
       $('.s_i-gallery-slider--counter span').eq(0).text(`0${currentSlide.currentSlide + 1}`);

   });
  
   sliderNavItem.on('click', function(e) {
       const self = $(this);
       _sliderSlideNext(parseInt($(this).data('gallery-slide')));
   });

   sliderItem.on('click', function(e) { _setImageBoxSrc($(this).data('slide-src')); });
});
