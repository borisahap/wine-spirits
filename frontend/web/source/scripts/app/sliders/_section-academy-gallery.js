$(() => {
    const slider = $('.s_a-gallery__slider');
    
    slider.slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        swipe: true,
        prevArrow: "<button class='slick-prev ui_button --outline --default --arrow'><i class='icon-arrow-left'></i></button>",
        nextArrow: "<button class='slick-next ui_button --outline --default --arrow'><i class='icon-arrow-right'></i></button>",
        responsive: [
            {
                breakpoint: BREAKPOINT_LG_MIN,
                settings: {
                    swipe: false
                }
            }
        ]
    }).on('afterChange', function(slick, currentSlide) {
        const slide = $(currentSlide.$slides[currentSlide.currentSlide]).find('.s_a-gallery__slider-item');
        $('.s_a-gallery__wrap').css({'background-image': `url("${slide.find('img').attr('src')}")`});
        $('.s_a-gallery--title').text(slide.data('title'));
        $('.s_a-gallery--text').text(slide.data('text'));
    });
});