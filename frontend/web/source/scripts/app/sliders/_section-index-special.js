$(() => {
    const slider = $('.s_i-special-slider');

    slider.slick({
        dots: false,
        infinite: true,
        
        prevArrow: "<button class='slick-prev ui_button --outline --default --arrow'><i class='icon-arrow-left'></i></button>",
        nextArrow: "<button class='slick-next ui_button --outline --default --arrow'><i class='icon-arrow-right'></i></button>",
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 0,
                settings: {
                    speed: 400,
                    swipe: true,
                    fade: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    adaptiveHeight: true
                }
            },
            {
                breakpoint: BREAKPOINT_LG_MIN,
                settings: {
                    speed: 750,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerMode: true,
                    centerPadding: '0',
                    variableWidth: true,
                }
            },
        ]
    });
    // _sliderWheelScroll(slider);
});