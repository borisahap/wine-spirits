// NOTE: Event slider mouse wheel scroll
const _sliderWheelScroll = slider => {
    let scrollRight = null;
    slider.on('wheel', (function (e) {
      e.preventDefault();
      if (scrollRight == null || scrollRight) {
        e.originalEvent.deltaY < 0 ? slider.slick('slickPrev') : slider.slick('slickNext');
        scrollRight = false;
        return;
      } else {
        setTimeout(() => { scrollRight = true; }, 300);
      }
      return;
    }));
  };
  
//= ./sliders/_section-index-special.js
//= ./sliders/_section-index-gallery.js
//= ./sliders/_section-academy-gallery.js