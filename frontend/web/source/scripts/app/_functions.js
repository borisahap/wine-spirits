// get windows params
let getWindowParams = () => {
    windowWidth = window.innerWidth;
    windowHeight = window.innerHeight;
    if(DEBUGGER) { $('.ui_debugger').css({'display': 'block'}); }
    return windowWidth, windowHeight; 
};


// get console logger
let getConsoleLog = () => {
    console.groupCollapsed('CONSOLELOG');
    // --------
    console.groupCollapsed('Window params');
    console.log('Height:', windowHeight, 'Width:', windowWidth);
    console.log('@media:',
        window.getComputedStyle(document.querySelector('.ui_debugger-item.--media'), '::after')
        .getPropertyValue('content').toUpperCase().split('"').join(''));
    console.groupEnd();
    // --------
    console.groupCollapsed('User info');
    console.info(window.navigator.userAgent);
    console.groupEnd();
    console.groupEnd();
    return false;
};
