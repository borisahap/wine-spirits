let map;
let mapPointMap = [];
function initMapShop() {
    let styledMapType = new google.maps.StyledMapType(
        [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            }
        ], { name: 'Styled Map' });

    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 53.904964, lng: 27.568004 },
        zoom: 7,
        disableDefaultUI: true,
        mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                'styled_map']
        }

    });


    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
    setMarkers(map);
  
}


function setMarkers(map) {


    var image = {
        url: '/img/point.png',
        size: new google.maps.Size(24, 24),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 24)
    };
    var shape = {
        coords: [1, 1, 1, 24, 22, 24, 22, 1],
        type: 'poly'
    };
    for (var i = 0; i < mappoint.length; i++) {
        var point = mappoint[i];
        var marker = new google.maps.Marker({
            position: { lat: point[1], lng: point[2] },
            map: map,
            icon: image,
            shape: shape,
            title: point[0],
            zIndex: point[3]
        });
        var contentString = `<div class="c_map-box"><img class="c_map-box--image" src="${point[4]}" /><div class="c_map-box--title">${point[0]}</div></div>`;

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 
            return function() {
                $('.gm-style-iw button').trigger('click');
                map.setZoom(15);
                map.setCenter(marker.getPosition());
                infowindow.open(map,marker);
            };
        })(marker,infowindow)); 
        mapPointMap.push(marker);
    }

}


$(() => {
    $('.c_map-card--goto[data-map-id]').on('click', function(e) {
        e.preventDefault();
        let id = parseInt($(this).data('map-id'));
        $('.gm-style-iw button').trigger('click');
        map.setCenter({ lat: mappoint[id][1], lng: mappoint[id][2] });
        map.setZoom(15);
    });
    $('.c_map-card--location').on('click', function(e) {
        e.preventDefault();
        $('body').addClass('.js_map-open');
        $('.s_s-map__wrap').addClass('--show');
        $(this).closest('.c_map-card').find('.c_map-card--goto').trigger('click');
    });
    $('.s_s-map__wrap--close').on('click', function(e) {
        $('body').removeClass('.js_map-open');
        $('.s_s-map__wrap').removeClass('--show');
    });
});
