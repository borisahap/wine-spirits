$(() => {
    const newsCatalogLink = $('[data-news-id]');
    const newsCatalogWrap = $('[data-news]');

    newsCatalogLink.on('click', function(e) {
        const self = $(this);
        const menuId = self.data('news-id');
        newsCatalogLink.removeClass('--current');
        self.addClass('--current');
        newsCatalogWrap.removeClass('--show');
        $(`.s_n-catalog__contents[data-news="${menuId}"]`).addClass('--show');
    });
});