$(() => {
    const posterLink = $('.s_p-posters-item');
    const posterArticle = $('.s_p-posters-article');

    posterLink.on('click', function(e) {
        if (windowWidth > BREAKPOINT_LG_MIN) {
            e.preventDefault();
            const self = $(this);
            const getPosterId = self.data('poster-id');
            posterLink.removeClass('--current');
            self.addClass('--current');
            posterArticle.removeClass('--current');
            $(`.s_p-posters-article[data-posters="${getPosterId}"]`).addClass('--current');
        }
    });
});