$(() => {
    const navPanel = $('.c_nav-panel');
    const navPanelList = navPanel.children();
    const navPanelSections = $('[data-navpanel-hash]');

    // NOTE: SET dots navPanel
    const _setNavPanelDot = (hash, name, current) => {
        const itemList = $(`<li class="c_nav-panel__list--item"></li>`);
        const itemListLink = $(`<a class="c_nav-panel__list--link" href=${hash}></a>`)
            .on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $('.c_nav-panel__list--link').removeClass('--current');
                setTimeout(() => { itemListLink.addClass('--current'); }, 100);
                $('html, body').stop().animate({
                    scrollTop: $(`[data-navpanel-hash="${hash}"]`).offset().top
                }, 500);
            }).tooltip({ container: 'body', boundary: 'window', title: name, placement: 'left', trigger: 'hover' });
        if (current != undefined && current != false) { itemListLink.addClass('--current'); }
        itemList.append(itemListLink);
        return itemList;
    };

    // NOTE: INIT navPanel
    const _initNavPanel = () => {
        let itemCount = 0;
        for (let item of navPanelSections) {
            const section = $(item);
            const getSectionHash = section.data('navpanel-hash');
            const getSectionName = section.data('navpanel-title');
            navPanelList.append(_setNavPanelDot(getSectionHash, getSectionName, itemCount == 0 ? true : false));
            itemCount++;
        }
    };


    // NOTE: get style (position => right) nav-panel
    const _getNavPanelStyle = () => {
        const alpha = 10;
        const containerWidth = $('.container').eq(0).innerWidth();
        const positionRight = ((windowWidth - containerWidth) / 2) - navPanel.innerWidth() - alpha;
        navPanel.css({ 'right': positionRight });
        return;
    };


    // NOTE: get style (class => color) nav-panel 
    navPanelSections.scrollie({
        scrollOffset: -(windowHeight / 1.8),
        scrollRatio: 2,
        scrollingInView: function (elem) {
            const windowInView = elem.data('navpanel-hash');
            // const navPanelIsDark = elem.data('navpanel') == 'dark' ? true : false;
            const navPanelItemCurrent = $(`.c_nav-panel__list--link[href="${windowInView}"]`);
            // navPanelIsDark ? navPanel.addClass('--dark') : navPanel.removeClass('--dark');
            $('.c_nav-panel__list--link').removeClass('--current');
            navPanelItemCurrent != undefined ? navPanelItemCurrent.addClass('--current') : false;
        }
    });

    // NOTE: INIT nav-panel
    _initNavPanel();
    _getNavPanelStyle();
    $(window).resize(() => _getNavPanelStyle());
});

