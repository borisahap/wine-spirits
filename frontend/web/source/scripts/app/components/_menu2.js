$(() => {
    const menuMain = $('.s_m-main__content');
    menuMain.slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
        swipe: true,
        mobileFirst: true,
        variableWidth: true,
        swipeToSlide: true,       
        responsive: [
            {
                breakpoint: BREAKPOINT_MD_MIN,
                settings: {
                    centerMode: true,
                    slidesToShow: 3,
                    centerPadding: '0',
                }
            },
            {
                breakpoint: BREAKPOINT_XL_MIN,
                settings: {
                    centerMode: true,
                    slidesToShow: 4,
                    centerPadding: '0 0 105%',
                }
            },
            {
                breakpoint: BREAKPOINT_XG_MIN,
                settings: {
                    centerMode: true,
                    slidesToShow: 4,
                    centerPadding: '0 0 125%',
                }
            }
        ]
    });
    _sliderWheelScroll(menuMain);


    const menuLink = $('[data-menu-id]');
    const menuWrap = $('[data-menu-wrap]');
    const menuAside = $(' .s_m-content__aside');

    menuLink.on('click', function(e) {
        e.preventDefault();
        const self = $(this);
        const menuId = self.data('menu-id');
        menuLink.removeClass('--current');
        self.addClass('--current');
        menuWrap.removeClass('--show');
        $(`.s_m-item[data-menu-wrap="${menuId}"]`).addClass('--show');
        menuAside.removeClass('--show');
    });

    $('.s_m-content--aside').on('click', function(e) {
        menuAside.addClass('--show');
    });
});