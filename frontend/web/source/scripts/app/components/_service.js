$(() => {
    const serviceSlider = $('.section-other-service-slider');
    serviceSlider.slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        swipe: false,
        mobileFirst: true,
        variableWidth: true,
        // swipeToSlide: true,       
        responsive: [
            {
                breakpoint: BREAKPOINT_MD_MIN,
                settings: {
                    slidesToShow: 2,
                }
            }
        ]
    });
    _sliderWheelScroll(serviceSlider);
    
});