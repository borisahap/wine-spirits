$(() => {
    const datePickerToggle = $("[data-datepicker-toggle]");
    const datePicker = $('.c_datepicker');
    const datePickerWrap = $('.c_datepicker-wrap');
    const datePickerClose = $('.c_datepicker--close');
    datePickerWrap.datepicker({
        maxViewMode: 0,
        language: "ru",
        toggleActive: true,
        todayHighlight: true,
        startDate: new Date(),
    }).on('changeDate', function(e) {
       datePickerToggle.find('input').val(
            datePickerWrap.datepicker('getFormattedDate')
       );
       datePicker.removeClass('--show');
    });

    datePickerToggle.on('click focus', function(e) {
        $(this).next().toggleClass('--show');
    });

    datePickerClose.on('click', function(e) {
        datePicker.removeClass('--show');
    });
});

;(function($){
    $.fn.datepicker.dates['ru'] = {
        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
        daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        today: "Сегодня",
        clear: "Очистить",
        format: "dd.mm.yyyy",
        weekStart: 1,
    monthsTitle: 'Месяцы'
    };
}(jQuery));