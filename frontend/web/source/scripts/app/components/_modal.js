$(() => {
    const modalPriceInput = $('[data-price-input]');
    const modalPriceValue = $('[data-price-value]');

    const modalPriceCost = parseInt(modalPriceValue.data('price-value'));

    modalPriceInput.keyup(function() {
        const count = parseInt($(this).val());
        if (count <= 0 && !isNaN(count) ) {
            $(this).val(1);
            modalPriceValue.val(modalPriceCost);
        } else {
            if (isNaN(count)) {
                modalPriceValue.val(modalPriceCost);
            } else {
                modalPriceValue.val(count * modalPriceCost );
            }
        }
    });

});