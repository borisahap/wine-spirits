// load
$(window).on('load', () => {
    console.info('---Load page---');
});

// ready
$(() => {
    console.info('---Ready page---');
    getWindowParams();
    if(CONSOLELOG) { getConsoleLog(); } 
});
// resize
$(window).resize(() => {
    console.info('---Resize page---');
    getWindowParams();
    if(CONSOLELOG) { getConsoleLog(); } 
});

