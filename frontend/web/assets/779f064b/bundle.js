/* jshint -W097 */
'use strict';

var BREAKPOINT_VX_MAX = 344.98;
var BREAKPOINT_XS_MAX = 575.98;
var BREAKPOINT_SM_MIN = 576;
var BREAKPOINT_SM_MAX = 767.98;
var BREAKPOINT_MD_MIN = 768;
var BREAKPOINT_MD_MAX = 991.98;
var BREAKPOINT_LG_MIN = 992;
var BREAKPOINT_LG_MAX = 1199.98;
var BREAKPOINT_XL_MIN = 1200;
var BREAKPOINT_XL_MAX = 1699.98;
var BREAKPOINT_XG_MIN = 1700;
var windowWidth;
var windowHeight;
var CONSOLELOG = true;
var DEBUGGER = true; // get windows params

var getWindowParams = function getWindowParams() {
  windowWidth = window.innerWidth;
  windowHeight = window.innerHeight;

  if (DEBUGGER) {
    $('.ui_debugger').css({
      'display': 'block'
    });
  }

  return windowWidth, windowHeight;
}; // get console logger


var getConsoleLog = function getConsoleLog() {
  console.groupCollapsed('CONSOLELOG'); // --------

  console.groupCollapsed('Window params');
  console.log('Height:', windowHeight, 'Width:', windowWidth);
  console.log('@media:', window.getComputedStyle(document.querySelector('.ui_debugger-item.--media'), '::after').getPropertyValue('content').toUpperCase().split('"').join(''));
  console.groupEnd(); // --------

  console.groupCollapsed('User info');
  console.info(window.navigator.userAgent);
  console.groupEnd();
  console.groupEnd();
  return false;
}; // load


$(window).on('load', function () {
  console.info('---Load page---');
}); // ready

$(function () {
  console.info('---Ready page---');
  getWindowParams();

  if (CONSOLELOG) {
    getConsoleLog();
  }
}); // resize

$(window).resize(function () {
  console.info('---Resize page---');
  getWindowParams();

  if (CONSOLELOG) {
    getConsoleLog();
  }
});
$(function () {
  var navPanel = $('.c_nav-panel');
  var navPanelList = navPanel.children();
  var navPanelSections = $('[data-navpanel-hash]'); // NOTE: SET dots navPanel

  var _setNavPanelDot = function _setNavPanelDot(hash, name, current) {
    var itemList = $("<li class=\"c_nav-panel__list--item\"></li>");
    var itemListLink = $("<a class=\"c_nav-panel__list--link\" href=".concat(hash, "></a>")).on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
      $('.c_nav-panel__list--link').removeClass('--current');
      setTimeout(function () {
        itemListLink.addClass('--current');
      }, 100);
      $('html, body').stop().animate({
        scrollTop: $("[data-navpanel-hash=\"".concat(hash, "\"]")).offset().top
      }, 500);
    }).tooltip({
      container: 'body',
      boundary: 'window',
      title: name,
      placement: 'left',
      trigger: 'hover'
    });

    if (current != undefined && current != false) {
      itemListLink.addClass('--current');
    }

    itemList.append(itemListLink);
    return itemList;
  }; // NOTE: INIT navPanel


  var _initNavPanel = function _initNavPanel() {
    var itemCount = 0;
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = navPanelSections[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var item = _step.value;
        var section = $(item);
        var getSectionHash = section.data('navpanel-hash');
        var getSectionName = section.data('navpanel-title');
        navPanelList.append(_setNavPanelDot(getSectionHash, getSectionName, itemCount == 0 ? true : false));
        itemCount++;
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator["return"] != null) {
          _iterator["return"]();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }; // NOTE: get style (position => right) nav-panel


  var _getNavPanelStyle = function _getNavPanelStyle() {
    var alpha = 10;
    var containerWidth = $('.container').eq(0).innerWidth();
    var positionRight = (windowWidth - containerWidth) / 2 - navPanel.innerWidth() - alpha;
    navPanel.css({
      'right': positionRight
    });
    return;
  }; // NOTE: get style (class => color) nav-panel 


  navPanelSections.scrollie({
    scrollOffset: -(windowHeight / 1.8),
    scrollRatio: 2,
    scrollingInView: function scrollingInView(elem) {
      var windowInView = elem.data('navpanel-hash'); // const navPanelIsDark = elem.data('navpanel') == 'dark' ? true : false;

      var navPanelItemCurrent = $(".c_nav-panel__list--link[href=\"".concat(windowInView, "\"]")); // navPanelIsDark ? navPanel.addClass('--dark') : navPanel.removeClass('--dark');

      $('.c_nav-panel__list--link').removeClass('--current');
      navPanelItemCurrent != undefined ? navPanelItemCurrent.addClass('--current') : false;
    }
  }); // NOTE: INIT nav-panel

  _initNavPanel();

  _getNavPanelStyle();

  $(window).resize(function () {
    return _getNavPanelStyle();
  });
});
$(function () {
  var datePickerToggle = $("[data-datepicker-toggle]");
  var datePicker = $('.c_datepicker');
  var datePickerWrap = $('.c_datepicker-wrap');
  var datePickerClose = $('.c_datepicker--close');
  datePickerWrap.datepicker({
    maxViewMode: 0,
    language: "ru",
    toggleActive: true,
    todayHighlight: true,
    startDate: new Date()
  }).on('changeDate', function (e) {
    datePickerToggle.find('input').val(datePickerWrap.datepicker('getFormattedDate'));
    datePicker.removeClass('--show');
  });
  datePickerToggle.on('click focus', function (e) {
    $(this).next().toggleClass('--show');
  });
  datePickerClose.on('click', function (e) {
    datePicker.removeClass('--show');
  });
});
;

(function ($) {
  $.fn.datepicker.dates['ru'] = {
    days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
    daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
    daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
    months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
    monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
    today: "Сегодня",
    clear: "Очистить",
    format: "dd.mm.yyyy",
    weekStart: 1,
    monthsTitle: 'Месяцы'
  };
})(jQuery);

$(function () {
  $('.s_header__nav--close, .s_header__nav--menu').on('click', function (e) {
    e.preventDefault();
    $('.s_header__nav').toggleClass('--show');
    $('body').toggleClass('js_menu-open');
  });
});
$(function () {
  var posterLink = $('.s_p-posters-item');
  var posterArticle = $('.s_p-posters-article');
  posterLink.on('click', function (e) {
    if (windowWidth > BREAKPOINT_LG_MIN) {
      e.preventDefault();
      var self = $(this);
      var getPosterId = self.data('poster-id');
      posterLink.removeClass('--current');
      self.addClass('--current');
      posterArticle.removeClass('--current');
      $(".s_p-posters-article[data-posters=\"".concat(getPosterId, "\"]")).addClass('--current');
    }
  });
});
$(function () {
  var menuMain = $('.s_m-main__content');
  menuMain.slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: false,
    swipe: true,
    mobileFirst: true,
    variableWidth: true,
    swipeToSlide: true,
    responsive: [{
      breakpoint: BREAKPOINT_MD_MIN,
      settings: {
        centerMode: true,
        slidesToShow: 3,
        centerPadding: '0'
      }
    }, {
      breakpoint: BREAKPOINT_XL_MIN,
      settings: {
        centerMode: true,
        slidesToShow: 4,
        centerPadding: '0 0 105%'
      }
    }, {
      breakpoint: BREAKPOINT_XG_MIN,
      settings: {
        centerMode: true,
        slidesToShow: 4,
        centerPadding: '0 0 125%'
      }
    }]
  });

  _sliderWheelScroll(menuMain);

  var menuLink = $('[data-menu-id]');
  var menuWrap = $('[data-menu-wrap]');
  var menuAside = $(' .s_m-content__aside');
  menuLink.on('click', function (e) {
    e.preventDefault();
    var self = $(this);
    var menuId = self.data('menu-id');
    menuLink.removeClass('--current');
    self.addClass('--current');
    menuWrap.removeClass('--show');
    $(".s_m-item[data-menu-wrap=\"".concat(menuId, "\"]")).addClass('--show');
    menuAside.removeClass('--show');
  });
  $('.s_m-content--aside').on('click', function (e) {
    menuAside.addClass('--show');
  });
});
var map;

function initMap() {
  var styledMapType = new google.maps.StyledMapType([{
    "featureType": "all",
    "elementType": "labels.text.fill",
    "stylers": [{
      "saturation": 36
    }, {
      "color": "#000000"
    }, {
      "lightness": 40
    }]
  }, {
    "featureType": "all",
    "elementType": "labels.text.stroke",
    "stylers": [{
      "visibility": "on"
    }, {
      "color": "#000000"
    }, {
      "lightness": 16
    }]
  }, {
    "featureType": "all",
    "elementType": "labels.icon",
    "stylers": [{
      "visibility": "off"
    }]
  }, {
    "featureType": "administrative",
    "elementType": "geometry.fill",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 20
    }]
  }, {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 17
    }, {
      "weight": 1.2
    }]
  }, {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 20
    }]
  }, {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 21
    }]
  }, {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 17
    }]
  }, {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 29
    }, {
      "weight": 0.2
    }]
  }, {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 18
    }]
  }, {
    "featureType": "road.local",
    "elementType": "geometry",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 16
    }]
  }, {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 19
    }]
  }, {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [{
      "color": "#000000"
    }, {
      "lightness": 17
    }]
  }], {
    name: 'Styled Map'
  });
  var coods;

  if (window.innerWidth > 767.98) {
    coods = {
      lat: 53.902854,
      lng: 27.557426
    };
  } else {
    coods = {
      lat: 53.902758,
      lng: 27.555559
    };
  }

  map = new google.maps.Map(document.getElementById('map'), {
    center: coods,
    zoom: 17,
    disableDefaultUI: true,
    mapTypeControlOptions: {
      mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
    }
  });
  var marker = new google.maps.Marker({
    position: {
      lat: 53.902758,
      lng: 27.555559
    },
    map: map,
    icon: '/img/map-point.png'
  });
  map.mapTypes.set('styled_map', styledMapType);
  map.setMapTypeId('styled_map');
}

$(function () {
  $('.s_o-contact__form--close, .s_o-contact--form').on('click', function (e) {
    e.preventDefault();
    $('.s_o-contact__form').toggleClass('--show');
  });
});
$(function () {
  var serviceSlider = $('.section-other-service-slider');
  serviceSlider.slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    swipe: false,
    mobileFirst: true,
    variableWidth: true,
    // swipeToSlide: true,       
    responsive: [{
      breakpoint: BREAKPOINT_MD_MIN,
      settings: {
        slidesToShow: 2
      }
    }]
  });

  _sliderWheelScroll(serviceSlider);
});
$(function () {
  var newsCatalogLink = $('[data-news-id]');
  var newsCatalogWrap = $('[data-news]');
  newsCatalogLink.on('click', function (e) {
    var self = $(this);
    var menuId = self.data('news-id');
    newsCatalogLink.removeClass('--current');
    self.addClass('--current');
    newsCatalogWrap.removeClass('--show');
    $(".s_n-catalog__contents[data-news=\"".concat(menuId, "\"]")).addClass('--show');
  });
});
$(function () {
  var pmodal = $('[data-pmodal-id]');
  var pmodalClose = $('[data-pmodal-close]');
  var pmodalOpen = $('div[data-pmodal-event]');
  pmodalOpen.on('click', function (e) {
    pmodal.removeClass('--show');
    $(".c_modal-page[data-pmodal-id=\"".concat($(this).data('pmodal-event'), "\"]")).toggleClass('--show');
    $('body').toggleClass('js_pmodal-open');
    $(document).keyup(function (e) {
      if (e.keyCode === 27) pmodalClose.trigger('click');
    });
  });
  pmodalClose.on('click', function (e) {
    pmodal.removeClass('--show');
    $('body').removeClass('js_pmodal-open');
  });
  $('.s_o-service-item').on('click', function (e) {
    console.log('asfasdfsa');
  });
}); // NOTE: Event slider mouse wheel scroll

var _sliderWheelScroll = function _sliderWheelScroll(slider) {
  var scrollRight = null;
  slider.on('wheel', function (e) {
    e.preventDefault();

    if (scrollRight == null || scrollRight) {
      e.originalEvent.deltaY < 0 ? slider.slick('slickPrev') : slider.slick('slickNext');
      scrollRight = false;
      return;
    } else {
      setTimeout(function () {
        scrollRight = true;
      }, 300);
    }

    return;
  });
};

$(function () {
  var slider = $('.s_i-special-slider');
  slider.slick({
    dots: false,
    infinite: true,
    prevArrow: "<button class='slick-prev ui_button --outline --default --arrow'><i class='icon-arrow-left'></i></button>",
    nextArrow: "<button class='slick-next ui_button --outline --default --arrow'><i class='icon-arrow-right'></i></button>",
    mobileFirst: true,
    responsive: [{
      breakpoint: 0,
      settings: {
        speed: 400,
        swipe: true,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true
      }
    }, {
      breakpoint: BREAKPOINT_LG_MIN,
      settings: {
        speed: 750,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '0',
        variableWidth: true
      }
    }]
  }); // _sliderWheelScroll(slider);
});
$(function () {
  var sliderItem = $('.s_i-gallery-slider--item');
  var sliderImageBox = $('.s_i-gallery__imagebox img');
  var slider = $('.s_i-gallery-slider');
  var sliderNavItem = $('.s_i-gallery-slider-nav--item');

  var _sliderSlideNext = function _sliderSlideNext(numSlide) {
    sliderNavItem.removeClass('--current');
    $(".s_i-gallery-slider-nav--item[data-gallery-slide='".concat(numSlide, "']")).addClass('--current');
    slider.slick('slickGoTo', numSlide);
  };

  var _setImageBoxSrc = function _setImageBoxSrc(img) {
    return sliderImageBox.attr('src', img);
  };

  slider.slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    swipe: true,
    prevArrow: '.s_i-gallery-slider--prev',
    nextArrow: '.s_i-gallery-slider--next',
    adaptiveHeight: true,
    responsive: [{
      breakpoint: BREAKPOINT_LG_MIN,
      settings: {
        swipe: false
      }
    }]
  }).on('afterChange', function (slick, currentSlide) {
    _sliderSlideNext(currentSlide.currentSlide);

    $('.s_i-gallery-slider--counter span').eq(0).text("0".concat(currentSlide.currentSlide + 1));
  });
  sliderNavItem.on('click', function (e) {
    var self = $(this);

    _sliderSlideNext(parseInt($(this).data('gallery-slide')));
  });
  sliderItem.on('click', function (e) {
    _setImageBoxSrc($(this).data('slide-src'));
  });
});
$(function () {
  var slider = $('.s_a-gallery__slider');
  slider.slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    swipe: true,
    prevArrow: "<button class='slick-prev ui_button --outline --default --arrow'><i class='icon-arrow-left'></i></button>",
    nextArrow: "<button class='slick-next ui_button --outline --default --arrow'><i class='icon-arrow-right'></i></button>",
    responsive: [{
      breakpoint: BREAKPOINT_LG_MIN,
      settings: {
        swipe: false
      }
    }]
  }).on('afterChange', function (slick, currentSlide) {
    var slide = $(currentSlide.$slides[currentSlide.currentSlide]).find('.s_a-gallery__slider-item');
    $('.s_a-gallery__wrap').css({
      'background-image': "url(\"".concat(slide.find('img').attr('src'), "\")")
    });
    $('.s_a-gallery--title').text(slide.data('title'));
    $('.s_a-gallery--text').text(slide.data('text'));
  });
});
//# sourceMappingURL=bundle.js.map
