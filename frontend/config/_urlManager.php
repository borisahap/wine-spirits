<?php

use Sitemaped\Sitemap;

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        //Pages
        ['pattern' => 'contact' , 'route' => 'site/contact'],
        ['pattern' => 'shop' , 'route' => 'shop/index'],
        ['pattern' => 'about' , 'route' => 'site/about'],
        ['pattern' => 'service' , 'route' => 'site/service'],
        ['pattern' => 'shops' , 'route' => 'site/shops'],

        //Menu
        ['pattern' => 'menu' , 'route'=> 'menu/index'],
        ['pattern' => 'menu/<menu>' , 'route'=> 'menu/view'],

        //Afisha
        ['pattern' => 'poster/order' , 'route' => 'poster/order'],
        ['pattern' => 'afisha/<month:\d+>' , 'route' => 'poster/index'],
        ['pattern' => 'afisha/<slug>' , 'route' => 'poster/view'],

        //Course
        ['pattern' => 'academy' , 'route'=> 'course/index'],
        ['pattern' => 'course/order' , 'route'=> 'course/order'],
        ['pattern' => 'academy/<slug>' , 'route'=> 'course/view'],

        // Articles
        ['pattern' => 'article', 'route' => 'article/index'],
        ['pattern' => 'article/attachment-download', 'route' => 'article/attachment-download'],
        ['pattern' => '<category>/<slug>', 'route' => 'article/view'],



        // Sitemap
        ['pattern' => 'sitemap.xml', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML]],
        ['pattern' => 'sitemap.txt', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_TXT]],
        ['pattern' => 'sitemap.xml.gz', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML, 'gzip' => true]],

    ]
];
