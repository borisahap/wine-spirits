<?php
/**
 * @author Eugene Terentev <eugene@terentev.net>
 */

$cache =
    [
    'class' => 'yii\caching\FileCache',
    'cachePath' => '@frontend/runtime/cache'
];
//    [
//
//        'class' => yii\caching\MemCache::class,
//
//        'servers' => [
//            [
//                'host' => 'localhost',
//                'port' => 11211,
//                'weight' => 100,
//            ],
//        ]
//    ];

if (YII_ENV_DEV) {
    $cache = [
        'class' => 'yii\caching\DummyCache'
    ];
//    $cache =
//    [
//    'class' => 'yii\caching\FileCache',
//    'cachePath' => '@frontend/runtime/cache'
//];
//        [
//
//            'class' => yii\caching\MemCache::class,
//            'servers' => [
//                [
//                    'host' => 'localhost',
//                    'port' => 11211,
//                    'weight' => 100,
//                ],
//            ]
//        ];
}

return $cache;
