<?php

namespace frontend\controllers;

use cheatsheet\Time;
use common\models\CallBack;
use common\models\Gallery;
use common\models\Hall;
use common\models\Hot;
use common\models\Order;
use common\models\Poster;
use common\models\Service;
use common\models\Team;
use common\sitemap\UrlsIterator;
use frontend\models\ContactForm;
use function GuzzleHttp\Promise\all;
use Sitemaped\Element\Urlset\Urlset;
use Sitemaped\Sitemap;
use Yii;
use yii\filters\PageCache;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => PageCache::class,
                'only' => ['sitemap'],
                'duration' => Time::SECONDS_IN_AN_HOUR,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $posters = Poster::find()
            ->where(['status' => Poster::STATUS_PUBLISHED])
            ->orderBy(['date' => SORT_DESC])
            ->limit(3)
            ->all();

        $gallery = Gallery::find()
            ->with('imgGalleries')
            ->all();

        $hots = Hot::find()
            ->where(['status' => Hot::STATUS_PUBLISHED])
            ->andWhere(['>=', 'date_start', time()])
            ->orderBy(['date_start' => SORT_ASC])
            ->all();

        if (Yii::$app->request->post()) {
            $model = new Order();
            $post = Yii::$app->request->post();
            $post['Order']['date'] = strtotime($post['Order']['date']);
            $model->load($post);
            if ($model->validate()) {
                $model->save();
                Yii::$app->session->setFlash('success','Заявка принята, с Вами скоро свяжутся');
                return $this->redirect(Yii::$app->request->referrer.'#order');
            } else {
                $errors = $model->errors;
                Yii::$app->session->setFlash('errors', $errors);
                Yii::$app->session->setFlash('oldData',$post['Order']);
                return $this->redirect(Yii::$app->request->referrer.'#order');

            }

        }

        return $this->render('index', [
            'posters' => $posters,
            'gallery' => $gallery,
            'hots' => $hots
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionContact()
    {
        $this->layout = '_clear';
        $model = new CallBack();
        if (Yii::$app->request->post()) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save();

                return $this->redirect(Yii::$app->request->referrer);
            } else {
                $errors = $model->errors;
                Yii::$app->session->setFlash('errors', $errors);
                Yii::$app->session->setFlash('oldForm',Yii::$app->request->post()['CallBack']);
                return $this->redirect(Yii::$app->request->referrer.'#form');

            }
        }
        return $this->render('contact', [
            'model' => $model
        ]);
    }

    /**
     * @param string $format
     * @param bool $gzip
     * @return string
     * @throws BadRequestHttpException
     */
    public function actionSitemap($format = Sitemap::FORMAT_XML, $gzip = false)
    {
        $links = new UrlsIterator();
        $sitemap = new Sitemap(new Urlset($links));

        Yii::$app->response->format = Response::FORMAT_RAW;

        if ($gzip === true) {
            Yii::$app->response->headers->add('Content-Encoding', 'gzip');
        }

        if ($format === Sitemap::FORMAT_XML) {
            Yii::$app->response->headers->add('Content-Type', 'application/xml');
            $content = $sitemap->toXmlString($gzip);
        } else if ($format === Sitemap::FORMAT_TXT) {
            Yii::$app->response->headers->add('Content-Type', 'text/plain');
            $content = $sitemap->toTxtString($gzip);
        } else {
            throw new BadRequestHttpException('Unknown format');
        }

        $linksCount = $sitemap->getCount();
        if ($linksCount > 50000) {
            Yii::warning(\sprintf('Sitemap links count is %d'), $linksCount);
        }

        return $content;
    }

    public function actionAbout()
    {

        $halls = Hall::find()->all();
        $team = Team::find()->all();

        return $this->render('about', [
            'halls' => $halls,
            'team' => $team
        ]);
    }

    public function  actionService()
    {
        $services = Service::find()->all();
        return $this->render('service',[
            'services' => $services
        ]);
    }
}
