<?php
/**
 * Created by PhpStorm.
 * User: JuniorPHP
 * Date: 22.04.2019
 * Time: 14:05
 */

namespace frontend\controllers;


use common\models\Courses;
use common\models\GeneralInfoCourses;
use common\models\OrderCourse;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CourseController extends Controller
{
    public function actionIndex()
    {
        $courses = Courses::find()
            ->where(['status'=>Courses::STATUS_PUBLISHED])
            ->all();
        $sliders = GeneralInfoCourses::find()->all();
        return $this->render('index',[
            'sliders' => $sliders,
            'courses' => $courses
        ]);
    }
    public function actionView($slug)
    {
        $model = Courses::find()
        ->where(['slug'=>$slug,'status'=>Courses::STATUS_PUBLISHED])
        ->with('programCourses','teacher')
        ->one();
        if (!$model){
            throw new NotFoundHttpException();
        }
        $model->views++;
        $model->update(false);
        return $this->render('view',[
            'model' => $model
        ]);
    }

    public function actionOrder()
    {
        if (\Yii::$app->request->isAjax) {
            $post = \Yii::$app->request->post();
            $model = new OrderCourse();
            $model->load($post);
            if ($model->save()) {
                return true;
            } else {
                $errors = $model->errors;
                return $this->asJson($errors);
            }
        }
    }
}