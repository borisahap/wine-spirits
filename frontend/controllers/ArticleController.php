<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\ArticleAttachment;
use common\models\ArticleCategory;
use common\models\Poster;
use frontend\models\search\ArticleSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class ArticleController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $news = Article::find()
            ->published()
            ->andWhere(['category_id' => 1])
            ->orderBy(['published_at' => SORT_DESC])
            ->all();
        $posters = Poster::find()
            ->where(['between', 'date', strtotime("first day of this month"), strtotime("last day of this month")])
            ->andWhere(['status' => Poster::STATUS_PUBLISHED])
            ->all();

        $articles = Article::find()
            ->published()
            ->andWhere(['category_id' => 2])
            ->orderBy(['published_at' => SORT_DESC])
            ->all();
        return $this->render('index', [
            'posters' => $posters,
            'news' => $news,
            'articles' => $articles
        ]);
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($category,$slug)
    {
        $category = ArticleCategory::findOne(['slug' => $category]);
        if (!$category){
            throw new NotFoundHttpException();
        }
        $model = Article::find()
            ->published()
            ->andWhere(['category_id' => $category->id,'slug'=>$slug])
            ->one();
       if (!$model){
           throw new NotFoundHttpException();
       }

       $articles = Article::find()
           ->published()
           ->andWhere(['category_id' => $category->id])
           ->andWhere(['!=','id',$model->id])
           ->all();


        return $this->render('view', [
            'model' => $model,
            'category' => $category,
            'articles' => $articles
        ]);
    }

    /**
     * @param $id
     * @return $this
     * @throws NotFoundHttpException
     * @throws \yii\web\HttpException
     */
    public function actionAttachmentDownload($id)
    {
        $model = ArticleAttachment::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException;
        }

        return Yii::$app->response->sendStreamAsFile(
            Yii::$app->fileStorage->getFilesystem()->readStream($model->path),
            $model->name
        );
    }
}
