<?php
/**
 * Created by PhpStorm.
 * User: JuniorPHP
 * Date: 15.04.2019
 * Time: 15:00
 */

namespace frontend\controllers;


use common\models\OrderPoster;
use common\models\Poster;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PosterController extends Controller
{
    public function actionIndex($month)
    {
        if ($month == null or $month > 12 or $month < 1) {

            throw new NotFoundHttpException;
        }
        $query = $posters = Poster::find()
            ->where(['status' => Poster::STATUS_PUBLISHED]);
        if ($month == date('m')) {
            $query->andWhere(['between', 'date', strtotime("first day of this month"), strtotime("last day of this month")]);

        } else {
            if ($month > date('m')) {
                $number = $month - date('m');
                $query->andWhere(['between', 'date', strtotime("first day of +" . $number . " month"), strtotime("last day of +" . $number . " month")]);
            }
            if ($month < date('m')) {
                $number = date('m') - $month;
                $query->andWhere(['between', 'date', strtotime("first day of -" . $number . " month"), strtotime("last day of -" . $number . " month")]);
            }
        }
        $query->orderBy(['date' => SORT_ASC]);
        $posters = $query->all();
        return $this->render('index', [
            'posters' => $posters
        ]);
    }

    public function actionView($slug)
    {

        $poster = Poster::find()
            ->where(['slug' => $slug])
            ->with('programPosters')
            ->one();
        if (!$poster) {
            throw new NotFoundHttpException();
        }

        $posts = Poster::find()
            ->where(['!=','id',$poster->id])
            ->orderBy(['id'=>SORT_DESC])
            ->limit(2)
            ->all();

        return $this->render('view', [
            'poster' => $poster,
            'posts' => $posts
        ]);
    }

    public function actionOrder()
    {
        if (\Yii::$app->request->isAjax) {
            $post = \Yii::$app->request->post();
            $model = new OrderPoster();
            $post['OrderPoster']['totalPrice'] = $post['OrderPoster']['count']*Poster::findOne($post['OrderPoster']['poster_id'])->price;
            $model->load($post);
            if ($model->save()) {
                return true;
            } else {
                $errors = $model->errors;
                return $this->asJson($errors);
            }
        }
    }
}