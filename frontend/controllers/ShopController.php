<?php
/**
 * Created by PhpStorm.
 * User: JuniorPHP
 * Date: 25.04.2019
 * Time: 8:59
 */

namespace frontend\controllers;


use common\models\Shop;
use yii\web\Controller;

class ShopController extends Controller
{

    public function actionIndex()
    {
        $shops = Shop::find()->all();
        return $this->render('shop',[
            'shops' => $shops
        ]);
    }
}