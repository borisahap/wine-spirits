<?php
/**
 * Created by PhpStorm.
 * User: JuniorPHP
 * Date: 17.04.2019
 * Time: 15:52
 */

namespace frontend\controllers;


use common\models\Menu;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class MenuController extends Controller
{

    public function actionIndex()
    {
        $menu = Menu::find()
            ->where(['status' => Menu::STATUS_PUBLISHED])
            ->andWhere(['is','parent_id', null])
            ->all();

        return $this->render('index', [
            'menu' => $menu
        ]);
    }

    public function actionView($menu)
    {
        $main_cat = Menu::findOne(['slug' => $menu]);
        if (!$main_cat) {
            throw new NotFoundHttpException();
        }
        $cats = Menu::find()
            ->where(['parent_id' => null])
            ->all();
        $child_cats = Menu::find()
            ->where(['parent_id' => $main_cat->id, 'status' => Menu::STATUS_PUBLISHED])
            ->with('products')
            ->all();

        return $this->render('view', [
            'main_cat' => $main_cat,
            'cats' => $cats,
            'child_cats' => $child_cats,
        ]);
    }
}