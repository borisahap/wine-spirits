<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use common\assets\Html5shiv;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Frontend application asset
 */
class CustomAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/web/bundle';

    /**
     * @var array
     */
    public $css = [
        'https://fonts.googleapis.com/css?family=Cormorant:300,400,500,700|Roboto+Condensed:300,400,700&amp;amp;subset=cyrillic',
        '/bundle/style.css',
    ];

    /**
     * @var array
     */
    public $js = [
//        '/bundle/_jquery-3.3.1.min.js',
        '/bundle/_underscore-min.js',
        '/bundle/_slick.min.js',
        '/bundle/_jquery.scrollie.js',
        'https://unpkg.com/popper.js@1.14.7/dist/umd/popper.min.js',
        '/bundle/_bootstrap.min.js',
        '/bundle/_bootstrap-datepicker.js',
        '/bundle/bundle.js',
        'https://cdn.rawgit.com/gisu/selectivizr/1.0.3/selectivizr.js',
        'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class,
        Html5shiv::class,
    ];
}
