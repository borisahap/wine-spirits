<?php
use common\widgets\MainMenuWidget;
use yii\helpers\Url;
$this->registerCssFile('/bundle/page-academy.css', ['depends' => ['frontend\assets\CustomAsset']]);

?>
<!-- content-->
<!-- promo-->
<section class="section-academy-promo" data-navpanel-hash="#academy" data-navpanel="white" data-navpanel-title="W&amp;S Academy">
    <div class="s_header__wrap container s_a-promo__wrap">
        <?php echo MainMenuWidget::widget() ?>
        <div class="s_header__content s_a-promo__content">
            <div class="s_a-promo--title">WINE & SPIRITS ACADEMY</div>
            <div class="s_a-promo--subtext">Ведущая образовательная платформа в области вин и крепких напитков.</div>
            <div class="s_a-promo--text">Концепция WINE & SPIRITS Academy предполагает ряд курсов различных уровней сложности для частных клиентов. Начиная от основ, заканчивая профессиональными тонкостями,  программы дают полный спектр знаний о винах и крепких напитках.</div>
        </div>
    </div>
</section>
<!-- courses-->
<section class="section-academy-courses" data-navpanel-hash="#course" data-navpanel="dark" data-navpanel-title="Курсы">
    <div class="container s_a-courses">
        <div class="line-container--wrap"></div>
        <div class="c_title --dark">
            <h5 class="c_title--text">Курсы</h5>
            <div class="c_title--subtext">Учись с нами</div>
        </div>
        <div class="s_a-courses__content s_news-card-grid">
            <?php if (!empty($courses)) : ?>
            <?php foreach ($courses as $course) : ?>
            <div class="c_news-card">
                <div class="c_news-card__content">
                    <div class="c_news-card--info"><span class="_year"><?= Yii::$app->formatter->asDate($course->date,'yyyy')?></span><span class="_date"><?= Yii::$app->formatter->asDate($course->date,'dd.MM.yyyy')?></span><span class="_views">просмотров: <?= $course->views ?></span></div><a class="c_news-card--title" href="<?=Url::toRoute(['/course/view','slug'=>$course->slug])?>"><?=$course->name?></a>
                    <div class="c_news-card--desc"><?= $course->description ?></div>
                    <div class="c_news-card--price"> <span class="_info"><?= $course->price == 0 ? 'Условия проведения обучения оговариваются индивидуально для каждого клиента.' : $course->price.' BYN' ?>  </span></div><a class="c_news-card--link ui_button --outline --secondary" href="<?=Url::toRoute(['/course/view','slug'=>$course->slug]).'#program'?>">учебная программа</a>
                </div>
                <div class="c_news-card__imagebox"><img src="<?=$course->getImg()?>" alt="<?=$course->name?>" title="<?=$course->name?>"></div>
            </div>
            <?php endforeach; ?>
           <?php endif; ?>
        </div>
    </div>
</section>
<!-- gallery-->
<?php if (!empty($sliders)) : ?>
<section class="section-academy-gallery" data-navpanel-hash="#gallery" data-navpanel="dark" data-navpanel-title="Галерея">
    <div class="container s_a-gallery">
        <div class="line-container--wrap"></div>
        <div class="s_a-gallery__head">
            <div class="s_a-gallery--title"><?= $sliders[0]->name ?></div>
            <div class="s_a-gallery--text"><?=$sliders[0]->description?></div>
        </div>
    </div>
    <div class="s_a-gallery__wrap" style="background-image: url(&quot;<?=$sliders[0]->getImg()?>&quot;)">
        <div class="container line-container-pseudo"></div>
        <div class="container s_a-gallery__inner">
            <div class="s_a-gallery__slider">
                <?php foreach ($sliders as $slider) : ?>
                <div class="s_a-gallery__slider-item" data-title="<?=$slider->name?>" data-text="<?= $slider->description ?>"><img src="<?=$slider->getImg()?>" alt="<?=$slider->name?>" title="<?=$slider->name?>"></div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<!-- END content-->
