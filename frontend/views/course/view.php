<?php
use common\widgets\MainMenuWidget;
use yii\helpers\Url;
$this->registerCssFile('/bundle/page-academy.css', ['depends' => ['frontend\assets\CustomAsset']]);
?>
<!-- content-->
<!-- promo-->
<section class="section-course-promo" style="background-image: url(&quot;<?=$model->getImg()?>&quot;)">
    <div class="s_header__wrap container s_c-promo__wrap">
        <?php echo MainMenuWidget::widget() ?>
        <div class="s_header__content s_c-promo__content">
            <ul class="s_c-promo__breadcrumbs c_breadcrumbs__list">
                <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="/">Главная</a></li>
                <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="<?=Url::toRoute(['/course/index'])?>">W&S Academy</a></li>
                <li class="c_breadcrumbs__list--item"><?= $model->name?></li>
            </ul>
            <div class="s_c-promo__title">
                <button class="s_c-promo--back ui_button --transparent" type="button"> <i class="icon-small-arrow-left"></i></button>
                <div class="s_c-promo--title"><?= $model->name?></div>
            </div>
        </div>
    </div>
</section>
<!-- info-->
<section class="section-course-info">
    <div class="s_c-info container">
        <div class="line-container--wrap" id="program"></div>
        <div class="s_c-info__wrap">
            <div class="s_c-info__content">
                <div class="s_c-info__list-wrap">
                    <div class="s_c-info__list--title">На курсе вы узнаете:</div>
                    <?= $model->result ?>
                </div>
                <div class="s_c-info__list-wrap">
                    <div class="s_c-info__list--title">После курсов вы сможете:</div>
                    <?= $model->after_course?>
                </div>
                <div class="s_c-info__list-wrap">
                    <div class="s_c-info__list--title">Как проходит обучение?</div>
                    <?=$model->trainig ?>
                </div>
            </div>
            <div class="s_c-info__teacher">
                <div class="c_command-card" data-prof="лектор">
                    <div class="c_command-card__imagebox"><img src="<?=$model->teacher->getImg()?>" alt="<?=$model->teacher->name?>" title="<?=$model->teacher->name?>"></div>
                    <div class="c_command-card--name"><?=$model->teacher->name?></div>
                    <div class="c_command-card--desc"><?=$model->teacher->description?></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- programm-->
<section class="section-course-program">
    <div class="s_c-program container">
        <div class="line-container--wrap"></div>
        <div class="s_c-program--title">Учебная программа</div>
        <div class="s_c-program__content">
            <?php $i=1?>
            <?php foreach ($model->programCourses as  $program) : ?>
            <div class="s_c-program-item">
                <div class="s_c-program-item--count"><?= $i<10 ? 0 : ''?><?=$i?></div>
                <div class="s_c-program-item--date"><?=Yii::$app->formatter->asDate($program->date,'dd.MM.yyyy')?></div>
                <div class="s_c-program-item--title"><?=$program->name?></div>
                <div class="s_c-program-item--text">
                   <?= $program->description ?>
                </div>
            </div>
            <?php $i++ ?>
            <?php endforeach; ?>
            <?= $model->exam === 1 ? '<div class="s_c-program-item --end">
                Экзамен</div>' : '' ?>
        </div>
    </div>
</section>
<!-- price-->
<section class="section-course-price">
    <div class="s_c-price container">
        <div class="line-container--wrap"></div>
        <div class="s_c-price__content">
            <div class="s_c-price--title">Стоимость обучения</div>
            <ul class="s_c-price__list">
                <li class="s_c-price__list-item"> <span>Время проведения:</span><?=$model->time?></li>
                <li class="s_c-price__list-item"> <span>Место проведения:</span><?=$model->place?></li>
                <li class="s_c-price__list-item"> <span>Номер телефона:</span><a href="tel:<?=$model->phone?>"><?=$model->phone?></a></li>
            </ul>
            <div class="s_c-price__info">
                <?php if ($model->price != 0 ) : ?>
                <div class="s_c-price__info--value"><?=$model->price?> BYN</div>
                <div class="s_c-price__info--subtext">(занятий: <?=$model->count?>)</div>
                <div class="s_c-price__info--text"><?= $model->note ?></div>
                <?php else: ?>
                <div class="s_c-price__info--text">Условия проведения обучения оговариваются индивидуально для каждого клиента.</div>
                <?php endif;?>
            </div>
            <button class="s_c-price--button ui_button --outline --light" type="button" data-toggle="modal" data-target="#courseOrder" >записаться</button>
        </div>
    </div>
</section>
<!-- END content-->
<!-- modal-->
<div class="ui_modal modal fade" id="courseOrder" role="dialog">
    <div class="modal-dialog">
        <form class="modal-content" id="ajax_form" action="/" method="post">
            <div class="modal-header ui_modal__title">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" >
                <input class="_input" id="id5" type="text" hidden name="OrderCourse[course_id]" value="<?=$model->id?>">
                <div class="ui_modal--title">Записаться</div>
                <button class="ui_modal--close ui_button --transparent" type="button" data-dismiss="modal" aria-label="Close"><i class="icon-cross-out"></i></button>
            </div>
            <div class="modal-body ui_modal__course">
                <div class="ui_modal--success" style="display: none"> Ваша заявка принята, ожидайте, с вами свяжутся.</div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id5">
                            Представьтесь пожалуйста<span>*</span></label>
                        <input class="_input" id="id5" type="text" name="OrderCourse[name]">
                        <div class="_error"></div>
                    </div>

                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id5">
                            E-mail<span>*</span></label>
                        <input class="_input" id="id5" type="text" name="OrderCourse[email]">
                        <div class="_error"></div>
                    </div>

                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id5">
                            Телефон<span>*</span></label>
                        <input class="_input" id="id5" type="tel" name="OrderCourse[phone]">
                        <div class="_error"></div>
                    </div>

                </div>
                <div class="ui_form-group">
                    <div class="ui_form-input">
                        <label class="_label" for="id6">
                            Количество человек<span>*</span></label>
                        <input class="_input" id="id6" type="number" data-price-input name="OrderCourse[count]" min="1" max="30" value="1">
                        <div class="_error"></div>
                    </div>

                </div>
            </div>
            <div class="modal-footer ui_modal__footer">
                <button class="ui_modal--submit ui_button --default" type="submit">записаться</button>
            </div>
        </form>
    </div>
</div>
<!-- END modal-->
<?php $orderCourse = <<< JS
    $('.ui_modal--submit').click(function(e) {
        e.preventDefault();
      $.ajax({
      url: '/course/order',
      type: "post",
      data: $("#ajax_form").serialize(),
      success: function(res) {
        if (res != true){

            $.each(res, function(key,val) {
                $('input[name="OrderCourse['+ key +']"]').next().html(val);
            });
        } else {
            $('._error').html('');
            $('.modal-body').find('.ui_form-group').hide();
            $('.modal-footer').hide();
            $('.ui_modal--success').show();
            $('.ui_modal--title').text('Успех!');
        }
      },
      error: function(array) {
        console.log(array); 
      }
      });
    })
JS;
$this->registerJs($orderCourse);
?>
