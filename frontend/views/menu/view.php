<?php

use common\widgets\MainMenuWidget;
use yii\helpers\Url;

$this->registerCssFile('/bundle/page-menu.css', ['depends' => ['frontend\assets\CustomAsset']]);
?>
    <section class="section-menu-promo"
             style="background-image: url(&quot;<?= $main_cat->banner_image == null ? $main_cat->getImg() : $main_cat->getBanner() ?>&quot;)">
        <div class="s_header__wrap container s_m-promo__wrap">
            <?php echo MainMenuWidget::widget() ?>
            <div class="s_header__content s_m-promo__content">
                <ul class="s_m-promo__breadcrumbs c_breadcrumbs__list">
                    <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="/">Главная</a></li>
                    <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link"
                                                             href="<?= Url::toRoute(['/menu/index']) ?>">Меню</a></li>
                    <li class="c_breadcrumbs__list--item"><?= $main_cat->name ?></li>
                </ul>
                <div class="s_m-promo--title"><span><?= $main_cat->name ?></span></div>
            </div>
        </div>
    </section>
    <!-- nav-->
<?php if (!empty($cats)) : ?>
    <section class="section-menu-nav">
        <div class="s_m-nav container">
            <div class="s_m-nav__wrap">
                <?php foreach ($cats as $cat) : ?>
                    <a class="s_m-nav--link <?= $cat->slug === $main_cat->slug ? '--current' : '' ?> "
                       href="<?= Url::toRoute(['/menu/view', 'menu' => $cat->slug]) ?>"> <span><?= $cat->name ?></span></a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
    <!-- content-->
<?php if (!empty($child_cats)) : ?>
    <section class="section-menu-content">
        <div class="s_m-content container">
            <div class="line-container--wrap"></div>
            <div class="s_m-content__wrap">
                <div class="s_m-content__aside --show">
                    <ul class="s_m-content__list">
                        <?php $i = 0; ?>
                        <?php foreach ($child_cats as $child) : ?>
                            <li class="s_m-content__list--item">
                                <div class="s_m-content__list--link <?= $i == 0 ? '--current' : '' ?>"
                                     data-menu-id="<?= $child->id ?>"><?= $child->name ?></div>
                            </li>
                            <?php $i++ ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="s_m-content__contents">
                    <button class="s_m-content--aside ui_button --transparent" type="button"><i
                                class="icon-menu-second"></i></button>
                    <div class="s_m-item --show" data-menu-wrap="0">
                        <div class="s_m-item--title"><span><?= $child_cats[0]->name ?></span>
                            <?php if (!empty($child_cats[0]->products)) : ?>
                            <div class="s_m-item--count"><?=count($child_cats[0]->products)?> позиций</div>
                        </div>
                        <div class="s_m-item__content">
                            <?php foreach ($child_cats[0]->products as $product) : ?>
                                <div class="s_m-item--position">
                                    <div class="s_m-item--text"><?= $product->name ?>
                                    </div>
                                    <div class="s_m-item--price"><?= $product->price ?> BYN</div>
                                </div>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php foreach ($child_cats as $child) : ?>
                        <div class="s_m-item" data-menu-wrap="<?= $child->id ?>">
                            <div class="s_m-item--title"><span><?= $child->name ?></span>
                                <?php if (!empty($child->products)) : ?>
                                <div class="s_m-item--count"><?=count($child_cats[0]->products)?> позиций</div>
                            </div>
                            <div class="s_m-item__content">
                                <?php foreach ($child->products as $product) : ?>
                                    <div class="s_m-item--position">
                                        <div class="s_m-item--text"><?= $product->name ?>
                                        </div>
                                        <div class="s_m-item--price"><?= $product->price ?> BYN</div>
                                    </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>