<?php
use common\widgets\MainMenuWidget;
use yii\helpers\Url;
$this->registerCssFile('/bundle/page-menu.css', ['depends' => ['frontend\assets\CustomAsset']]);

?>

<section class="section-menu-main">
    <div class="container line-container-pseudo"></div>
    <div class="s_header__wrap container s_m-main__wrap">
        <?php echo MainMenuWidget::widget() ?>
    </div>
    <?php if (!empty($menu)) : ?>
    <div class="s_m-main__content">
        <?php foreach ($menu as $men) : ?>
        <div> <a class="s_m-main--link" href="<?= Url::toRoute(['/menu/view','menu'=>$men->slug])?>" style="background-image: url(&quot;<?=$men->getImg()?>&quot;)"> <span><?=$men->name?></span></a></div>
        <?php endforeach; ?>
    </div>
    <?php endif;?>
</section>
