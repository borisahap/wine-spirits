<?php

use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php')
?>


    <?php echo $content ?>

<?php $this->endContent() ?>
<footer class="section-footer">
    <div class="s_footer container"><a class="s_footer--logo" href="/"><img src="/img/logo.svg" alt="" title=""></a>
        <ul class="s_footer__nav">
            <li class="s_footer__nav--item"> <a class="s_footer__nav--link" href="<?=Url::toRoute(['/site/about'])?>">О нас</a></li>
            <li class="s_footer__nav--item"> <a class="s_footer__nav--link" href="<?=Url::toRoute(['/poster/index','month'=>date('m')])?>">Афиша</a></li>
            <li class="s_footer__nav--item"> <a class="s_footer__nav--link" href="<?= Url::toRoute(['/course/index'])?>">W&S Academy</a></li>
            <li class="s_footer__nav--item"> <a class="s_footer__nav--link" href="<?=Url::toRoute(['/menu/index'])?>">Меню</a></li>
            <li class="s_footer__nav--item"> <a class="s_footer__nav--link" href="<?=Url::toRoute(['/article/index'])?>">W&S News</a></li>
            <li class="s_footer__nav--item"> <a class="s_footer__nav--link" href="<?=Url::toRoute(['/site/service'])?>">Услуги</a></li>
            <li class="s_footer__nav--item"> <a class="s_footer__nav--link" href="<?=Url::toRoute(['/shop/index'])?>">W&S Shop</a></li>
            <li class="s_footer__nav--item"> <a class="s_footer__nav--link" href="<?=Url::toRoute(['/site/contact'])?>">Контакты</a></li>
        </ul>
        <div class="s_footer__contact"> <a class="s_footer__contact--link ui_button --transparent --invert" href="tel:<?=Yii::$app->keyStorage->get('phone1')?>"> <i class="icon-phone-call" data-button-icon="left"></i><span><?=Yii::$app->keyStorage->get('phone1')?></span></a>
            <div class="s_footer__contact--link ui_button --transparent --invert --text"><i class="icon-time" data-button-icon="left"></i><span><?=Yii::$app->keyStorage->get('timework')?></span></div><a class="s_footer__contact--link ui_button --transparent --invert" href="#!"> <i class="icon-placeholder" data-button-icon="left"></i><span><?=Yii::$app->keyStorage->get('address')?></span></a>
        </div>
        <div class="s_footer__social">
            <div class="s_footer__social--title">Следите за нами:</div><a class="s_footer__social--link ui_button --transparent --invert" target="_blank" href="<?=Yii::$app->keyStorage->get('fb')?>"> <i class="icon-facebook"></i></a><a class="s_footer__social--link ui_button --transparent --invert" target="_blank" href="<?=Yii::$app->keyStorage->get('tripadvisor')?>"> <i class="icon-tripadvisor-logotype"></i></a><a class="s_footer__social--link ui_button --transparent --invert" target="_blank" href="<?=Yii::$app->keyStorage->get('insta')?>"> <i class="icon-instagram"></i></a>
        </div>
        <div class="s_footer__copyring">2019 Разработка сайта <a target="_blank" href="https://vg-group.pro">VG Group</a></div>
    </div>
</footer>
<!-- END footer-->
<!-- modal-->
<!-- END modal-->

<!-- navpanel-->
<div class="c_nav-panel">
    <ul class="c_nav-panel__list"></ul>
</div>
