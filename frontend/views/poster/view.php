<?php
use common\widgets\MainMenuWidget;
use yii\helpers\Url;
$this->registerCssFile('/bundle/page-poster.css', ['depends' => ['frontend\assets\CustomAsset']]);

Yii::$app->opengraph->title = $poster->meta_title;
Yii::$app->opengraph->url = Yii::$app->request->getAbsoluteUrl();
Yii::$app->opengraph->description = $poster->meta_description;
Yii::$app->opengraph->image = $poster->getImg();


?>
<section class="section-poster-head" style="background-image: url(&quot;<?= $poster->getImg() ?>&quot;)">
    <div class="s_header__wrap container s_p-head__wrap">
        <?php echo MainMenuWidget::widget() ?>
        <div class="s_header__content s_p-head__content">
            <div class="s_p-head__breadcrumbs c_breadcrumbs">
                <ul class="c_breadcrumbs__list">
                    <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="/">Главная</a></li>
                    <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="<?=Url::toRoute(['/poster/index','month'=>date('m')])?>">Афиши</a></li>
                    <li class="c_breadcrumbs__list--item"><?= $poster->name ?></li>
                </ul>
            </div>
            <div class="s_p-head__title">
                <button class="s_p-head--back ui_button --transparent" type="button"> <i class="icon-small-arrow-left"></i></button>
                <div class="s_p-head--date"><?=Yii::$app->formatter->asDate($poster->date,'dd.MM.yyyy')?></div>
                <div class="s_p-head--title"><?= $poster->name ?></div>
            </div>
        </div>
    </div>
</section>
<!--content-->
<section class="section-poster-content">
    <div class="container s_p-content">
        <div class="line-container--wrap"></div>
        <div class="s_p-content__wrap">
            <div class="s_p-content__social"> <span>поделиться</span><a href="http://vk.com/share.php?url=<?=Yii::$app->request->getAbsoluteUrl()?>&title=<?=$poster->meta_title?>&description=<?=$poster->meta_description?>&image=<?=$poster->getImg()?>&noparse=true"> <i class="icon-vk"></i></a><a href="https://www.facebook.com/sharer/sharer.php?u=<?=Yii::$app->request->getAbsoluteUrl()?>"> <i class="icon-facebook"></i></a></div>
            <div class="s_p-content__article">
                <?= $poster->description ?>
            </div>
            <?php if (!empty($poster->programPosters)) : ?>
            <div class="s_p-content__dinner">
                <div class="s_p-content__dinner--title">Программа мероприятия: </div>
                <?php foreach ($poster->programPosters as $program) : ?>
                <div class="s_p-content-dinner">
                    <div class="s_p-content-dinner__imagebox"><img src="<?= $program->getImg()?>" alt="<?= $program->name ?>" title="<?= $program->name ?>"></div>
                    <div class="s_p-content-dinner--title"><?= $program->name ?></div>
                    <div class="s_p-content-dinner--subtitle"><?= $program->to_name ?></div>
                    <div class="s_p-content-dinner--desc">
                        <?= $program->description ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
            <div class="s_p-content__price">
                <div class="s_p-content__price--cost">   <span>стоимость</span><span><?= $poster->price ?> BYN</span></div>
                <div class="s_p-content__price--contact">Предварительная бронь по телефону: <a href="tel:<?=$poster->phone?>"><?=$poster->phone?></a></div>
                <button class="s_p-content__price--order ui_button --default" type="button">забронировать</button>
            </div>
        </div>
    </div>
</section>
<!--events-->
<?php if (!empty($posts)) : ?>
<section class="section-poster-events">
    <div class="container s_p-events">
        <div class="line-container--wrap"></div>
        <div class="c_title --dark">
            <div class="c_title--text">Ближайшие мероприятия</div>
        </div>
        <div class="s_p-events__content s_news-card-grid">
            <?php foreach ($posts as $post) : ?>
            <div class="c_news-card">
                <div class="c_news-card__content">
                    <div class="c_news-card--date"><?= Yii::$app->formatter->asDate($post->date,'dd.MM.yyyy')?></div><a class="c_news-card--title" href="<?=Url::toRoute(['/poster/view','slug'=>$post->slug])?>"><?=$post->name?></a>
                    <div class="c_news-card--desc"><?= $post->description ?></div><a class="c_news-card--link ui_button --outline --secondary" href="<?=Url::toRoute(['/poster/view','slug'=>$post->slug])?>">читать</a>
                </div>
                <div class="c_news-card__imagebox"><img src="<?=$post->getImg()?>" alt="<?=$post->name?>" title="<?=$post->name?>"></div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>