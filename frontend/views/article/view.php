<?php
use common\widgets\MainMenuWidget;
use yii\helpers\Url;
use yii\helpers\StringHelper;
$this->registerCssFile('/bundle/page-poster.css', ['depends' => ['frontend\assets\CustomAsset']]);
$this->title = $model->meta_title != null ? $model->meta_title : strip_tags($model->title);
Yii::$app->opengraph->title = $this->title;
Yii::$app->opengraph->url = Yii::$app->request->getAbsoluteUrl();
Yii::$app->opengraph->description = $model->meta_description != null ? $model->meta_description : StringHelper::truncate(strip_tags($model->body), 150) ;
Yii::$app->opengraph->image = $model->getImg();
?>
<!-- content-->
<!--head-->
<section class="section-poster-head" style="background-image: url(&quot;<?= $model->getImg() ?>&quot;)">
    <div class="s_header__wrap container s_p-head__wrap">
        <?php echo MainMenuWidget::widget() ?>
        <div class="s_header__content s_p-head__content">
            <div class="s_p-head__breadcrumbs c_breadcrumbs">
                <ul class="c_breadcrumbs__list">
                    <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="/">Главная</a></li>
                    <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="<?= Url::toRoute(['/article/index'])?>"><?= $category->title ?></a></li>
                    <li class="c_breadcrumbs__list--item"><?=$model->title?></li>
                </ul>
            </div>
            <div class="s_p-head__title">
                <button class="s_p-head--back ui_button --transparent" type="button"> <i class="icon-small-arrow-left"></i></button>
                <div class="s_p-head--date"><?=Yii::$app->formatter->asDate($model->published_at, 'dd.MM.yyyy')?></div>
                <div class="s_p-head--title"><?=$model->title?></div>
            </div>
        </div>
    </div>
</section>
<!--content-->
<section class="section-poster-content">
    <div class="container s_p-content">
        <div class="line-container--wrap"></div>
        <div class="s_p-content__wrap">
            <div class="s_p-content__social"> <span>поделиться</span><a href="http://vk.com/share.php?url=<?=Yii::$app->request->getAbsoluteUrl()?>&title=<?=$model->meta_title?>&description=<?=$model->meta_description?>&image=<?=$model->getImg()?>&noparse=true"> <i class="icon-vk"></i></a><a href="https://www.facebook.com/sharer/sharer.php?u=<?=Yii::$app->request->getAbsoluteUrl()?>"> <i class="icon-facebook"></i></a></div>
            <div class="s_p-content__article">
                <?= $model->body ?>
            </div>
        </div>
    </div>
</section>
<!--events-->
<?php if (!empty($articles)) :?>
<section class="section-news-events">
    <div class="container s_n-events">
        <div class="line-container--wrap"></div>
        <div class="c_title --dark">
            <div class="c_title--text">Вас заинтерсует</div>
        </div>
        <div class="s_n-events__content">
            <?php foreach ($articles as $article) :?>
            <div class="c_poster-card">
                <div class="c_poster-card__wrap">
                    <div class="c_poster-card__tags">
                        <?php foreach (explode(' ',$article->tags) as $tag) : ?>
                        <a class="c_poster-card__tags--link" href="#!"><?=$tag?></a>
                        <?php endforeach;?>
                    </div><a class="c_poster-card--title" href="<?=Url::toRoute(['/article/view','category'=>$category->slug,'slug'=>$article->slug])?>"><?=$article->title?></a>
                    <div class="c_poster-card__desc">
                         <?=StringHelper::truncate($article->body,150)?>
                    </div>
                    <div class="c_poster-card__bottom"> <a class="c_poster-card--more ui_button --outline --secondary" href="<?=Url::toRoute(['/article/view','category'=>$category->slug,'slug'=>$article->slug])?>">подробнее</a>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>