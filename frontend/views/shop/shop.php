<?php
use common\widgets\MainMenuWidget;
$this->registerCssFile('/bundle/page-other.css', ['depends' => ['frontend\assets\CustomAsset']]);
?>
<!-- content-->
<!-- promo-->
<section class="section-shop-promo">
    <div class="s_header__wrap container s_s-promo__wrap">
        <?php echo MainMenuWidget::widget() ?>
        <div class="s_header__content s_s-promo__content">
            <ul class="s_s-promo__breadcrumbs c_breadcrumbs__list">
                <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="/">Главная</a></li>
                <li class="c_breadcrumbs__list--item">Магазины</li>
            </ul>
            <div class="s_s-promo--title">WINE & SPIRITS SHOP</div>
            <div class="s_s-promo--text">Магазины атмосферных напитков WINE & SPIRITS — собственная розничная сеть импортера и дистрибьютора алкогольной продукции GARSIA Group. Первый магазин распахнул свои двери для покупателей весной 2015 года. С тех пор сеть активно растет и развивается.</div>
        </div>
    </div>
</section>
<!-- features-->
<section class="section-shop-features">
    <div class="s_s-features container">
        <div class="line-container--wrap"></div>
        <div class="s_s-features__content">
            <div class="s_s-features-item">
                <div class="s_s-features-item--count">01</div>
                <div class="s_s-features-item--title">Широкий ассортимент</div>
                <div class="s_s-features-item--text">В наших магазинах представлены как известные мировые бренды, так и эксклюзивные позиции. В магазине представлена алкогольная продукция, которую вы не встретите на полках обычного супермаркета. Будьте уверены, что в WINE & SPIRITS Вы найдете алкоголь со всех уголков мира.</div>
            </div>
            <div class="s_s-features-item">
                <div class="s_s-features-item--count">02</div>
                <div class="s_s-features-item--title">Консультация</div>
                <div class="s_s-features-item--text">Каждый сотрудник WINE & SPIRITS — специалист высокого уровня, который постоянно совершенствует свои знания и навыки, проходя специализированное обучение. В магазинах WINE & SPIRITS помогут выбрать качественный продукт, который придется по вкусу вам или тому человеку, для которого вы выбрали подарок.</div>
            </div>
            <div class="s_s-features-item">
                <div class="s_s-features-item--count">03</div>
                <div class="s_s-features-item--title">Дегустация</div>
                <div class="s_s-features-item--text">В сети магазинов WINE & SPIRITS еженедельно проходят тематические дегустации. Наши опытные кависты помогут всем, кто искренне увлечен удивительным миром алкогольных напитков, окунуться в атмосферу неповторимых вкусов и культуру их потребления</div>
            </div>
        </div>
    </div>
</section>
<!-- map-->
<?php if (!empty($shops)) : ?>
<section class="section-shop-map">
    <div class="s_s-map container">
        <div class="line-container--wrap"></div>
        <div class="s_s-map__inner">
            <div class="s_s-map__shops">
                <?php foreach ($shops as $shop) : ?>
                <div class="c_map-card">
                    <button class="c_map-card--goto ui_button --default" type="button" data-map-id="<?= $shop->id ?>"><i class="icon-arrow-right"></i></button>
                    <div class="c_map-card__imagebox"><img src="<?=$shop->getImg()?>" alt="<?=$shop->name?>" title="<?=$shop->name?>"></div>
                    <div class="c_map-card__content">
                        <button class="c_map-card--location ui_button --transparent" type="button"> <i class="icon-placeholder"></i></button>
                        <div class="c_map-card--subtitle">WINE SPIRITS</div>
                        <div class="c_map-card--title"><?=$shop->name?></div>
                        <ul class="c_map-card__list">
                            <li class="c_map-card__list--item"> <span>Адрес</span>
                                <div><?=$shop->city?>, <?=$shop->street?></div>
                            </li>
                            <li class="c_map-card__list--item"> <span>Время работы</span>
                                <div><?=$shop->time?></div>
                            </li>
                            <li class="c_map-card__list--item"> <span>Телефон</span>
                                <div>
                                    <?php foreach (explode(', ',$shop->phone) as $phone) : ?>
                                    <a href="tel:<?=$phone?>"><?=$phone?></a>
                                    <?php endforeach; ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="s_s-map__wrap">
                <button class="s_s-map__wrap--close c_modal-page--close ui_button --transparent" type="button"><i class="icon-cross-out"></i></button>
                <div class="s_s-map__wrap-inner">
                    <div class="s_s-map__wrap-inner--iframe" id="map"></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var mappoint = [
            <?php foreach ($shops as $shop) : ?>
            [ '<?=$shop->name?>', <?=$shop->latitude?>, <?=$shop->longitude?>, <?=$shop->id?>, '<?=$shop->getImg()?>'],
           <?php endforeach; ?>
        ];
    </script><script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLXboo4QKtt2FR6Dq6nyu_rTR9Qi4NWZc&amp;callback=initMapShop&amp;language=ru" async defer ></script>
</section>
<?php endif; ?>
<!-- END content-->
