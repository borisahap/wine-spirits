<?php

use common\widgets\MainMenuWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */
$this->registerCssFile('/bundle/page-index.css', ['depends' => ['frontend\assets\CustomAsset']]);
$this->title = Yii::$app->name;
?>
<section class="section-index-promo" data-navpanel-hash="#welcome" data-navpanel="white" data-navpanel-title="Приветствие">
    <div class="s_i-promo__video">
        <video autoplay muted loop>
            <source src="<?= Yii::getAlias('@host_storage/video/wss.mp4')?>" type="video/mp4">
        </video>
    </div>
    <div class="s_header__wrap container s_i-promo__wrap">
        <?php echo MainMenuWidget::widget()?>
        <div class="s_header__content s_i-promo__content">
            <div class="s_i-promo__content--title">Добро пожаловать</div>
            <div class="s_i-promo__content--name"><img src="/img/studio.svg" alt="Wine &amp; Spirits Studio" title="Wine &amp; Spirits Studio"></div>
        </div>
    </div>
</section>
<main class="sections-index-page">
    <!-- features-->
    <section class="section-index-features" data-navpanel-hash="#features" data-navpanel="white" data-navpanel-title="Особенности">
        <div class="container s_i-features">
            <div class="line-container--wrap"></div>
            <div class="c_title --white">
                <div class="c_title--text">Особенности</div>
                <div class="c_title--subtext">наше превосходство</div>
            </div>
            <div class="s_i-features__grid">
                <div class="s_i-features-item"><i class="s_i-features-item--icon icon-bung"></i>
                    <div class="s_i-features-item--title">Пробковый сбор</div>
                    <div class="s_i-features-item--text">фиксированная цена на вино</div>
                </div>
                <div class="s_i-features-item"><i class="s_i-features-item--icon icon-cook"></i>
                    <div class="s_i-features-item--title">Ches’s table</div>
                    <div class="s_i-features-item--text">фиксированная цена на вино</div>
                </div>
                <div class="s_i-features-item"><i class="s_i-features-item--icon icon-wine"></i>
                    <div class="s_i-features-item--title">W&S Shop</div>
                    <div class="s_i-features-item--text">фиксированная цена на вино</div>
                </div>
                <div class="s_i-features-item"><i class="s_i-features-item--icon icon-sommelier"></i>
                    <div class="s_i-features-item--title">Школа сомелье</div>
                    <div class="s_i-features-item--text">фиксированная цена на винo</div>
                </div>
            </div>
        </div>
    </section>
    <!-- poster-->
    <?php if (!empty($posters)) : ?>
    <section class="section-index-poster" data-navpanel-hash="#poster" data-navpanel="dark" data-navpanel-title="Афиша">
        <div class="container s_i-poster">
            <div class="line-container--wrap"></div>
            <div class="c_title --dark">
                <h5 class="c_title--text">Aфиша</h5>
                <div class="c_title--subtext">ближайшие мероприятия</div>
            </div>
            <div class="s_i-poster__content">
                <?php foreach ($posters as $poster) : ?>
                <div class="c_poster-card" data-poster-date="<?= Yii::$app->formatter->asDate($poster->date,'dd.MM.yyyy')?>">
                    <div class="c_poster-card__wrap">
                        <div class="c_poster-card__tags">
                            <?php foreach (explode(' ',$poster->tags) as $tag) :?>
                            <a class="c_poster-card__tags--link" href="#!"><?= $tag ?></a>
                           <?php endforeach; ?>
                        </div><a class="c_poster-card--title" href="<?=Url::toRoute(['/poster/view','slug'=>$poster->slug])?>"><?=$poster->name?></a>
                        <div class="c_poster-card__desc">
                            <p> <?=$poster->description ?></p>
                        </div>
                        <div class="c_poster-card__bottom">
                            <div class="c_poster-card__price"><span><?=$poster->price?> </span><span>BYN</span></div><a class="c_poster-card--more ui_button --outline --secondary" href="#!">забронировать</a>
                        </div>
                    </div>
                </div>
               <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- specialoffers-->
    <?php if (!empty($hots)) : ?>
    <section class="section-index-specialoffers" data-navpanel-hash="#specialoffers" data-navpanel="white" data-navpanel-title="Спецпредложения">
        <div class="container line-container-pseudo"></div>
        <div class="container s_i-special">
            <div class="c_title --white">
                <h5 class="c_title--text">Спецпредложения</h5>
                <div class="c_title--subtext">ближайшие мероприятия</div>
            </div>
        </div>
        <div class="s_i-special__content container">
            <div class="s_i-special-slider">
                <?php foreach ($hots as $hot ) :?>
                <?php Yii::$app->formatter->locale = 'ru-RU'; ?>
                <div class="s_i-special-item__wrap" data-special-date="<?=Yii::$app->formatter->asDate($hot->date_start);?> - <?=Yii::$app->formatter->asDate($hot->date_finished);?>">
                    <a class="s_i-special-item" href="">
                        <div class="s_i-special-item--title"><?= $hot->name ?></div>
                        <div class="s_i-special-item__imagebox"> <img src="<?= $hot->getImg() ?>" alt="<?= $hot->name ?>" title="<?= $hot->name ?>"></div>
                        <div class="s_i-special-item__desc">
                            <p><?= \yii\helpers\StringHelper::truncate($hot->description,200) ?></p>
                        </div>
                        <div class="s_i-special-item__date"><?=Yii::$app->formatter->asDate($hot->date_start);?> - <?=Yii::$app->formatter->asDate($hot->date_finished);?></div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- gallery-->
    <?php if (!empty($gallery)) : ?>
    <section class="section-index-gallery" data-navpanel-hash="#gallery" data-navpanel="dark" data-navpanel-title="Фотогалерея">
        <div class="container line-container-pseudo"></div>
        <div class="container s_i-gallery">
            <div class="c_title --dark">
                <h5 class="c_title--text">Фотогалерея</h5>
                <div class="c_title--subtext">ближайшие мероприятия</div>
            </div>
            <div class="s_i-gallery__content">
                <div class="s_i-gallery--title">Уникальное мультиплатформенное пространство</div>
                <div class="s_i-gallery-slider">
                    <?php foreach ($gallery as $photo) : ?>
                    <div class="s_i-gallery-slider__wrap">
                        <?php if (isset($photo->imgGalleries)) : ?>
                        <?php foreach ($photo->imgGalleries as $img) : ?>
                        <div class="s_i-gallery-slider--item" data-slide-src="<?=$img->getImg()?>"><img src="<?=$img->getImg()?>" alt="<?=$photo->name?>" title="<?=$photo->name?>"></div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="s_i-gallery-slider-arrow">
                    <button class="s_i-gallery-slider--prev ui_button --transparent"><i class="icon-arrow-left"></i></button>
                    <div class="s_i-gallery-slider--counter"><span>01</span>/<span>0<?=count($gallery)?></span></div>
                    <button class="s_i-gallery-slider--next ui_button --transparent"><i class="icon-arrow-right"></i></button>
                </div>
                <div class="s_i-gallery__imagebox"><img src="<?=$gallery[0]->imgGalleries[0]->getImg()?>" alt="<?=$gallery[0]->name?>" title="<?=$gallery[0]->name?>"></div>
                <div class="s_i-gallery-slider-nav">
                    <?php $i = 0 ; ?>
                    <?php foreach ($gallery as $gal) : ?>
                    <div class="s_i-gallery-slider-nav--item <?= $i == 0 ? '--current' : '' ?>" data-gallery-slide="<?=$i ?>"><i class="icon-coffee-table"></i><span><?=$gal->name?></span></div>
                    <?php $i++;?>
                   <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- table reservation-->
    <section class="section-index-reservation" data-navpanel-hash="#reservation" data-navpanel="white" data-navpanel-title="Бронь стола">
        <div class="container line-container-pseudo"></div>
        <div class="container s_i-reservation">
            <div class="c_title --white">
                <h5 class="c_title--text">Бронь стола</h5>
                <div class="c_title--subtext">в гости к Studio</div>
            </div>
          <?php echo \common\widgets\OrderWidget::widget() ?>
        </div>
    </section>
</main>
