<?php
use common\widgets\MainMenuWidget;
use yii\helpers\StringHelper;
$this->registerCssFile('/bundle/page-other.css', ['depends' => ['frontend\assets\CustomAsset']]);

?>
<!-- content-->
<!-- service-->
<section class="section-other-service">
    <div class="s_header__wrap container s_o-service__wrap">
        <?= MainMenuWidget::widget() ?>
        <div class="s_header__content s_o-service__content">
            <ul class="s_o-service__breadcrumbs c_breadcrumbs__list">
                <li class="c_breadcrumbs__list--item"><a class="c_breadcrumbs__list--link" href="/">Главная</a></li>
                <li class="c_breadcrumbs__list--item">Сервис</li>
            </ul>
            <div class="s_o-service__card">
                <div class="s_o-service__card--title">Услуги</div>
                <div class="s_o-service__card--subtitle"><?=Yii::$app->keyStorage->get('banner_service')?></div>
                <div class="s_o-service__card--text">В рамках проекта Wine & Spirits Academy мы проводим множество тематических мероприятий — эногастрономические ужины, мастер-классы, экскурсы-дегустации с участием виноделов, мастеров купажей и бренд-амбассадоров со всего мира.</div>
            </div>
        </div>
    </div>
</section>
<?php if (!empty($services)) :?>
<section class="section-other-service-slider">
    <?php foreach ($services as $service) : ?>
    <div>
        <div class="s_o-service-item" data-pmodal-event="<?= $service->id?>" style="background-image: url(&quot;<?=$service->getImg()?>&quot;)">
            <div class="s_o-service-item--title"><?=$service->name?></div>
            <div class="s_o-service-item--text"><?= StringHelper::truncate($service->description,150) ?> </div>
            <div class="s_o-service-item--link ui_button --transparent"><i class="icon-arrow-right"></i></div>
        </div>
    </div>
   <?php endforeach; ?>
</section>
<?php foreach ($services as $service) :?>
    <div class="c_modal-page" data-pmodal-id="<?= $service->id?>">
        <button class="c_modal-page--close ui_button --transparent" type="button" data-pmodal-close><i class="icon-cross-out"></i></button>
        <div class="c_modal-page__content">
            <button class="c_modal-page--return ui_button --transparent" type="button" data-pmodal-close>вернуться на услуги</button>
            <div class="c_modal-page--logo"><img src="/img/logo-black.svg" alt="" title=""></div>
            <div class="c_modal-page--title"><?=$service->name?></div>
            <div class="c_modal-page__desc"><?= $service->description ?></div>
        </div>
        <div class="c_modal-page__imagebox" style="background-image: url(&quot;<?=$service->getImg()?>&quot;)"></div>
    </div>
    <?php endforeach; ?>
<?php endif;?>


<!-- END content-->
