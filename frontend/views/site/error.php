<?php

use yii\helpers\Html;
use common\widgets\MainMenuWidget;

$this->registerCssFile('/bundle/page-other.css', ['depends' => ['frontend\assets\CustomAsset']]);
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<section class="section-other-404">
    <div class="s_header__wrap container s_o-404__wrap">
        <?php echo MainMenuWidget::widget() ?>
        <div class="s_header__content s_o-404__content">
            <div class="s_o-404__message">
                <div class="s_o-404--title">Ой...</div>
                <div class="s_o-404--text">
                    Страница, которую Вы запросили,отсутствует на сайте.<br>Возможно, Вы ошиблись при наборе адреса или перешли по неверной ссылке</div><a class="s_o-404--link ui_button --default" href="/">вернуться на главную<i class="icon-arrow-right"></i></a>
            </div>
            <div class="s_o-404__imagebox"> <img src="/img/404.png" alt="" title=""></div>
        </div>
    </div>
</section>