<?php
use common\widgets\MainMenuWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->registerCssFile('/bundle/page-other.css', ['depends' => ['frontend\assets\CustomAsset']]);

?>
<section class="section-other-contact">
    <div class="s_o-contact__map">
        <div id="map"></div>
    </div>
    <div class="s_header__wrap container s_o-contact__wrap">
       <?= MainMenuWidget::widget() ?>
        <div class="s_header__content s_o-contact__content">
            <div class="s_o-contact__information">
                <div class="s_o-contact--title c_title">
                    <h5 class="c_title--text">Контакты</h5>
                </div>
                <ul class="s_o-contact__list">
                    <li class="s_o-contact__list--item">
                        <div>Наш адрес:</div>
                        <div><?= Yii::$app->keyStorage->get('address')?></div>
                    </li>
                    <li class="s_o-contact__list--item">
                        <div>Заказать столик:</div>
                        <div><a href="tel:<?=Yii::$app->keyStorage->get('phone1')?>"><?=Yii::$app->keyStorage->get('phone1')?></a></div>
                    </li>
                    <li class="s_o-contact__list--item">
                        <div>Ждем ваши вопросы и пожелания:</div>
                        <div>    <a href="mailto:<?=Yii::$app->keyStorage->get('email')?>"><?=Yii::$app->keyStorage->get('email')?></a></div>
                    </li>
                </ul>
                <button class="s_o-contact--form ui_button --outline --light" type="button"> <span>написать нам</span><i class="icon-arrow-right"></i></button>
                <div class="s_o-contact__social">
                    <ul class="s_o-contact__list">
                        <li class="s_o-contact__list--item">
                            <div>Следите за нами:</div>
                            <div> <a target="_blank" href="<?=Yii::$app->keyStorage->get('vk')?>"><i class="icon-vk"></i></a><a target="_blank" href="<?=Yii::$app->keyStorage->get('fb')?>"><i class="icon-facebook"></i></a><a target="_blank" href="<?=Yii::$app->keyStorage->get('insta')?>"><i class="icon-instagram"></i></a><a target="_blank" href="<?=Yii::$app->keyStorage->get('youtube')?>"><i class="icon-youtube"></i></a></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <form id="form" class="s_o-contact__form <?=Yii::$app->session->hasFlash('errors') ? '--show' : ''?>" method="post" action="<?=\yii\helpers\Url::toRoute(['/site/contact'])?>">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" >
            <button class="s_o-contact__form--close ui_button --transparent" type="button"><i class="icon-cross-out"> </i></button>
            <div class="s_o-contact__form--title c_title">
                <div class="c_title--text">Напишите нам</div>
            </div>
            <div class="ui_form-group">
                <div class="ui_form-input">
                    <label class="_label" for="id5">
                        Имя<span>*</span></label>
                    <input class="_input" id="id5" type="text" value="<?= Yii::$app->session->getFlash('oldForm') != null ? Yii::$app->session->getFlash('oldForm')['name'] : '' ?>" name="CallBack[name]">
                </div>
                <div class="_error" style="color: red;"><p><?= Yii::$app->session->getFlash('errors') != null && array_key_exists ('name',Yii::$app->session->getFlash('errors')) ? Yii::$app->session->getFlash('errors')['name'][0] : ''?></p></div>

            </div>
            <div class="ui_form-group">
                <div class="ui_form-input">
                    <label class="_label" for="id5">
                        Телефон<span>*</span></label>
                    <input class="_input" id="id5" type="text" value="<?= Yii::$app->session->getFlash('oldForm') != null ? Yii::$app->session->getFlash('oldForm')['phone'] : '' ?>" name="CallBack[phone]">
                </div>
                <div class="_error" style="color: red;"><p><?= Yii::$app->session->getFlash('errors') != null && array_key_exists ('phone',Yii::$app->session->getFlash('errors')) ? Yii::$app->session->getFlash('errors')['phone'][0] : ''?></p></div>

            </div>
            <div class="ui_form-group">
                <div class="ui_form-input">
                    <label class="_label" for="id7">Комментарии</label>
                    <textarea class="_textarea" name="CallBack[note]" id="id7" rows="4"><?= Yii::$app->session->getFlash('oldForm') != null ? Yii::$app->session->getFlash('oldForm')['note'] : '' ?></textarea>
                </div>
            </div>
            <button class="s_o-contact__form--submit ui_button --outline --light" type="submit">отправить</button>
        </form>
    </div>
</section><script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLXboo4QKtt2FR6Dq6nyu_rTR9Qi4NWZc&amp;callback=initMap&amp;language=ru" async defer ></script>
