<?php
use common\widgets\MainMenuWidget;
$this->registerCssFile('/bundle/page-about.css', ['depends' => ['frontend\assets\CustomAsset']]);
$class = ['icon-wine-shope', 'icon-coffee-table', 'icon-dinner', 'icon-pastry-chef'];
?>
<div class="sections-about-wrap">
    <!-- promo-->
    <section class="section-about-promo" data-navpanel-hash="#about" data-navpanel="white" data-navpanel-title="О нас">
        <div class="s_header__wrap container s_a-promo__wrap">
            <?= MainMenuWidget::widget()?>
            <div class="s_header__content s_a-promo__content">
                <div class="s_a-promo--logo"><img src="/img/studio2.svg" alt="" title=""></div>
                <div class="s_a-promo--text">
                    <p>Это уникальное для Беларуси мультиплатформенное пространство, объединяющее в себе несколько площадок: магазин, кафе-бар-ресторан, винную школу.</p>
                    <p>Данный проект — один из этапов развития группы компаний Garsia, которая на протяжении 25-ти лет является одним из крупнейших импортеров премиальных алкогольных напитков в Республику Беларусь и известна своей сетью магазиновWine & Spirits.</p>
                    <p>Миссия компании — развитие эногастрономической культуры.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- features-->
    <section class="section-about-features" data-navpanel-hash="#features" data-navpanel="white" data-navpanel-title="Преимущества">
        <div class="s_a-features container">
            <div class="line-container--wrap"></div>
            <div class="s_a-features--title c_title --white">
                <h5 class="c_title--text"> <span>Чем </span>мы отличаемся от других ресторанов</h5>
                <div class="c_title--subtext">уникальность</div>
            </div>
            <div class="s_a-features__content">
                <div class="s_a-features-item">
                    <div class="s_a-features-item--count"><span>01</span>Ассортимент</div>
                    <div class="s_a-features-item--text">Наша карта вин и крепких напитков — крупнейшая в стране и составляет более чем 300 наименований. Здесь также представлен ряд эксклюзивных позиций, которые можно приобрести только у нас.</div>
                </div>
                <div class="s_a-features-item">
                    <div class="s_a-features-item--count"><span>02</span>Цены</div>
                    <div class="s_a-features-item--text">Для наших гостей мы обеспечиваем уникальную ценовую политику и предоставляем возможность приобретать напитки по наиболее привлекательной стоимости.</div>
                </div>
                <div class="s_a-features-item">
                    <div class="s_a-features-item--count"><span>03</span>Сервис</div>
                    <div class="s_a-features-item--text">Все сотрудники нашего проекта проходят профессиональное обучение по направлению «кавист-консультант». Таким образом, мы обеспечиваем уникальный для Беларуси высококомпетентный экспертный сервис.</div>
                </div>
                <div class="s_a-features-item">
                    <div class="s_a-features-item--count"><span>04</span>Локация</div>
                    <div class="s_a-features-item--text">Мы расположены в самом сердце Минска — из окон нашего пространства открывается уникальный вид на исторический центр города, сквер площади Свободы, Ратушу и отель Европа.</div>
                </div>
                <div class="s_a-features-item">
                    <div class="s_a-features-item--count"><span>05</span>Кухня</div>
                    <div class="s_a-features-item--text">Концепция кухни —  «wine&spirits friendly cuisine» - блюда, наиболее гармонично сочетающиеся с винами и крепкими напитками. Представляем в нашем меню два стилевых вектора: «Классика и Тренды». </div>
                </div>
                <div class="s_a-features-item">
                    <div class="s_a-features-item--count"><span>06</span>Мероприятия</div>
                    <div class="s_a-features-item--text">Мы проводим множество тематических мероприятий — эногастрономические ужины, мастер-классы, экскурсы-дегустации с участием виноделов, мастеров купажей и бренд-амбассадоров со всего мира.</div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- content-->
<!-- halls-->
<section class="section-about-hall" data-navpanel-hash="#halls" data-navpanel="dark" data-navpanel-title="Залы">
    <div class="container s_a-hall">
        <div class="line-container--wrap"></div>
        <div class="c_title --dark">
            <div class="c_title--text">Залы</div>
            <div class="c_title--subtext">комфорт интерьера</div>
        </div>
        <div class="s_a-hall__content">
            <?php $i = 0 ?>
            <?php foreach ($halls as $hall) : ?>
                <div class="s_a-hall-card" data-pmodal-event="<?= $hall->id ?>">
                    <div class="s_a-hall-card--title"><i class="<?= $class[$i] ?>"></i><span><?= $hall->name ?></span>
                    </div>
                    <div class="s_a-hall-card--desc"><?= $hall->description ?></div>
                    <div class="s_a-hall-card--link ui_button --transparent"><i class="icon-arrow-right"></i></div>
                </div>
                <?php $i++ ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php $i = 0 ?>
<?php foreach ($halls as $hall) : ?>
    <div class="c_modal-page" data-pmodal-id="<?= $hall->id ?>">
        <button class="c_modal-page--close ui_button --transparent" type="button" data-pmodal-close><i
                    class="icon-cross-out"></i></button>
        <div class="c_modal-page__content">
            <div class="c_modal-page--logo"><img src="/img/logo-black.svg" alt="<?= $hall->name ?>"
                                                 title="<?= $hall->name ?>"></div>
            <div class="c_modal-page--halltitle"><i class="<?= $class[$i] ?>"></i><span><?= $hall->name ?></span></div>
            <div class="c_modal-page__desc"><?= $hall->description ?></div>
        </div>
        <div class="c_modal-page__imagebox" data-pmodal-imagecaption="<?= $hall->sub_name ?>"
             style="background-image: url(&quot;<?= $hall->getImg() ?>&quot;)"></div>
    </div>
    <?php $i++ ?>
<?php endforeach; ?>
<!-- command-->
<?php if (!empty($team)) : ?>
    <section class="section-about-command" data-navpanel-hash="#command" data-navpanel="dark"
             data-navpanel-title="Команда">
        <div class="container s_a-command">
            <div class="line-container--wrap"></div>
            <div class="c_title --dark">
                <div class="c_title--text">Наша команда</div>
                <div class="c_title--subtext">только мастера своего дела</div>
            </div>
            <div class="s_a-command__content">
                <?php foreach ($team as $person) : ?>
                    <div class="c_command-card" data-prof="<?= $person->position ?>">
                        <div class="c_command-card__imagebox"><img src="<?= $person->getImg() ?>"
                                                                   alt="<?= $person->name ?>"
                                                                   title="<?= $person->name ?>"></div>
                        <div class="c_command-card--name"><?= $person->name ?></div>
                        <div class="c_command-card--desc"><?= $person->description ?></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- END content-->
