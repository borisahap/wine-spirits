<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Hot;

/**
 * HotSearch represents the model behind the search form about `common\models\Hot`.
 */
class HotSearch extends Hot
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cat_id', 'status', 'date_start', 'date_finished'], 'integer'],
            [['name', 'description', 'image'], 'safe'],
            [['date_start', 'date_finished'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            [['date_start', 'date_finished'], 'default', 'value' => null],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hot::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'cat_id' => $this->cat_id,
            'status' => $this->status,
        ]);
        if ($this->date_start !== null) {
            $query->andFilterWhere(['date_start' => $this->date_start]);
        }

        if ($this->date_finished !== null) {
            $query->andFilterWhere(['date_finished'=> $this->date_finished]);
        }
        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);


        return $dataProvider;
    }
}
