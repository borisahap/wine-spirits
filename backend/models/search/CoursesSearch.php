<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Courses;

/**
 * CourseSearch represents the model behind the search form about `common\models\Course`.
 */
class CoursesSearch extends Courses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'exam', 'price', 'count', 'status', 'teacher_id'], 'integer'],
            [['meta_title', 'meta_description', 'name', 'image', 'description', 'result', 'after_course', 'trainig', 'place', 'phone', 'time', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Courses::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'exam' => $this->exam,
            'price' => $this->price,
            'count' => $this->count,
            'status' => $this->status,
            'teacher_id' => $this->teacher_id,
        ]);

        $query->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'after_course', $this->after_course])
            ->andFilterWhere(['like', 'trainig', $this->trainig])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'time', $this->time])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
