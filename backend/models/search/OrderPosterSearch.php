<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderPoster;

/**
 * OrderPosterSearch represents the model behind the search form about `common\models\OrderPoster`.
 */
class OrderPosterSearch extends OrderPoster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'poster_id', 'count', 'totalPrice', 'status'], 'integer'],
            [['name', 'phone', 'email', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderPoster::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'poster_id' => $this->poster_id,
            'count' => $this->count,
            'totalPrice' => $this->totalPrice,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
