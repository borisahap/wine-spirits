<?php

namespace backend\controllers;

use League\Uri\File;
use Yii;
use common\models\Menu;
use backend\models\search\MenuSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $parents = Menu::find()
            ->where(['parent_id' => null])
            ->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parents' => $parents
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();
        $parents = Menu::find()
            ->where(['parent_id' => null])
            ->all();
        array_unshift($parents, '');
        if ($model->load(Yii::$app->request->post())) {
            $model->saveImage();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
            'parents' => $parents
        ]);
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $parents = Menu::find()
            ->where(['parent_id' => null])
            ->all();
        array_unshift($parents, '');
        $oldImage = $model->image;
        $oldBanner = $model->banner_image;
        if ($model->load(Yii::$app->request->post())) {
            if (UploadedFile::getInstance($model, 'image') != null) {
                FileHelper::unlink(Yii::getAlias('@storage/web/menu-image/' . $oldImage));
                $model->saveImage();
            } else {
                $model->image = $oldImage;
            };
            if (UploadedFile::getInstance($model, 'banner_image') != null) {
                FileHelper::unlink(Yii::getAlias('@storage/web/menu-image/banner/' . $oldBanner));
                $model->saveImage();
            } else {
                $model->banner_image = $oldBanner;
            };

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
            'parents' => $parents
        ]);
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->image != null){
            FileHelper::unlink(Yii::getAlias('@storage/web/menu-image/'.$model->image));
        }
        if ($model->parent_id == null){
            foreach ($model->menus as $cat)
            {
                FileHelper::unlink(Yii::getAlias('@storage/web/menu-image/'.$cat->image));
                $cat->delete();
            }
        }
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
