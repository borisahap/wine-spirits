<?php

namespace backend\controllers;

use Yii;
use common\models\Hall;
use backend\models\search\HallSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * HallController implements the CRUD actions for Hall model.
 */
class HallController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hall models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HallSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Hall model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hall model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new Hall();
//
//        if (Yii::$app->request->post()) {
//            $post = Yii::$app->request->post();
//            $model->load($post);
//            $model->saveImage();
//            $model->save();
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//        return $this->render('create', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Updates an existing Hall model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldImage = $model->image;

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $model->load($post);
            if (UploadedFile::getInstance($model, 'image') != null) {
                FileHelper::unlink(Yii::getAlias('@storage/web/hall-image/' . $oldImage));
                $model->saveImage();
            } else {
                $model->image = $oldImage;
            };

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Hall model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $model = $this->findModel($id);
//        if ($model->image != null) {
//            FileHelper::unlink(Yii::getAlias('@storage/web/hall-image/' . $model->image));
//        }
//        $model->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the Hall model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hall the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hall::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
