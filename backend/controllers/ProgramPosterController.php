<?php

namespace backend\controllers;

use common\models\Poster;
use Yii;
use common\models\ProgramPoster;
use backend\models\search\ProgramPosterSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProgramPosterController implements the CRUD actions for ProgramPoster model.
 */
class ProgramPosterController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProgramPoster models.
     * @return mixed
     */
    public function actionIndex($post_id)
    {
        $poster = Poster::findOne($post_id);
        $searchModel = new ProgramPosterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'poster' => $poster
        ]);
    }

    /**
     * Displays a single ProgramPoster model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProgramPoster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProgramPoster();

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->saveImage();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProgramPoster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $model->load($post);
            if (UploadedFile::getInstance($model, 'image') != null) {
                FileHelper::unlink(Yii::getAlias('@storage/web/poster-program-image/' . $oldImage));
                $model->saveImage();
            } else {
                $model->image = $oldImage;
            };

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProgramPoster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->image != null) {
            FileHelper::unlink(Yii::getAlias('@storage/web/poster-program-image/' . $model->image));
        }

        $model->delete();


        return $this->redirect(['index']);
    }

    /**
     * Finds the ProgramPoster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProgramPoster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProgramPoster::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
