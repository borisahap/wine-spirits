<?php

namespace backend\controllers\course;

use common\models\ProgramCourse;
use common\models\Teachers;
use Yii;
use common\models\Courses;
use backend\models\search\CoursesSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CoursesController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new CoursesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Courses();
        $teachers = Teachers::find()->all();
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $post['Courses']['date'] = strtotime($post['Courses']['date']);
            $model->load($post);
            $model->saveImage();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
            'teachers' => $teachers
        ]);
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $teachers = Teachers::find()->all();

        $oldImage = $model->image;

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $post['Courses']['date'] = strtotime($post['Courses']['date']);
            $model->load($post);
            if (UploadedFile::getInstance($model, 'image') != null) {
                FileHelper::unlink(Yii::getAlias('@storage/web/course-image/courses-image/' . $oldImage));
                $model->saveImage();
            } else {
                $model->image = $oldImage;
            };

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
            'teachers' => $teachers
        ]);
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->image != null) {
            FileHelper::unlink(Yii::getAlias('@storage/web/course-image/courses-image/' . $model->image));
        }
        ProgramCourse::deleteAll(['course_id'=>$model->id]);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Courses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Courses::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
