<?php

namespace backend\controllers\product;

use Yii;
use common\models\Hot;
use backend\models\search\HotSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Menu;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * HotController implements the CRUD actions for Hot model.
 */
class HotController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HotSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $cats = Menu::find()
            ->where(['!=','parent_id', 0])
            ->andWhere(['!=','status',0])
            ->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cats' => $cats
        ]);
    }

    /**
     * Displays a single Hot model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Hot();
        $cats = Menu::find()
            ->where(['!=','parent_id', 0])
            ->andWhere(['!=','status',0])
            ->all();
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $post['Hot']['date_start'] = strtotime($post['Hot']['date_start']);
            $post['Hot']['date_finished'] = strtotime($post['Hot']['date_finished']);
            $model->load($post);
            $model->saveImage();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
            'cats' => $cats
        ]);
    }

    /**
     * Updates an existing Hot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $cats = Menu::find()
            ->where(['!=','parent_id', 0])
            ->andWhere(['!=','status',0])
            ->all();
        $oldImage = $model->image;

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $post['Hot']['date_start'] = strtotime($post['Hot']['date_start']);
            $post['Hot']['date_finished'] = strtotime($post['Hot']['date_finished']);
            $model->load($post);
            if (UploadedFile::getInstance($model, 'image') != null) {
                FileHelper::unlink(Yii::getAlias('@storage/web/menu-image/product-image/' . $oldImage));
                $model->saveImage();
            } else {
                $model->image = $oldImage;
            };

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
            'cats' => $cats
        ]);
    }

    /**
     * Deletes an existing Hot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->image != null) {
            FileHelper::unlink(Yii::getAlias('@storage/web/menu-image/product-image/' . $model->image));
        }
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Hot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hot::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
