<?php
/**
 * Created by PhpStorm.
 * User: JuniorPHP
 * Date: 11.04.2019
 * Time: 16:44
 */

namespace backend\controllers;


use common\components\keyStorage\FormModel;
use Yii;
use yii\web\Controller;

class InfoController extends Controller
{
    public function actionIndex()
    {
        $model = new FormModel([
            'keys' => [
                'Phone1' => [
                    'pole' => Yii::t('backend', 'Phone1'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'Phone2' => [
                    'pole' => Yii::t('backend', 'Phone2'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'Email' => [
                    'pole' => Yii::t('backend', 'Email'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'TimeWork' => [
                    'pole' => Yii::t('backend', 'TimeWork'),
                    'type' => FormModel::TYPE_TEXTAREA,
                ],
                'Address' => [
                    'pole' => Yii::t('backend', 'Address'),
                    'type' => FormModel::TYPE_TEXTAREA,
                ],
                'VK' => [
                    'pole' => Yii::t('backend', 'VK'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'Insta' => [
                    'pole' => Yii::t('backend', 'Insta'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'FB' => [
                    'pole' => Yii::t('backend', 'FB'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],

                'Youtube' => [
                    'pole' => Yii::t('backend', 'Youtube'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
                'TripAdvisor' => [
                    'pole' => Yii::t('backend', 'trip'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],

                'Support' => [
                    'pole' => Yii::t('backend', 'support'),
                    'type' => FormModel::TYPE_TEXTINPUT,
                ],
            ],
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', [
                'body' => Yii::t('backend', 'Настройки успешно сохранены'),
                'options' => ['class' => 'alert alert-success'],
            ]);

            return $this->refresh();
        }

        return $this->render('index', ['model' => $model]);
    }
}