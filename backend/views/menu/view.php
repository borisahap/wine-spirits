<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-view">

    <p>
        <?php echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'meta_title',
            'meta_description:ntext',
            'name',
            'description:ntext',
            [
                    'attribute' => 'parent_id',
                'value' =>isset($model->parent) ? $model->parent->name : 'no set',
            ],
            [
                    'attribute' => 'status',
                'value' => \common\models\Menu::statuses()[$model->status],
            ],
            [
                'attribute' => 'image',
                'value'=>$model->getImg(),
                 'format' => ['image',['width'=>'240','height'=>'auto']],

            ],
            [
                'attribute' => 'banner_image',
                'value'=>$model->getBanner(),
                'format' => ['image',['width'=>'240','height'=>'auto']],

            ],
        ],
    ]) ?>

</div>
