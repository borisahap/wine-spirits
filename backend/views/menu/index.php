<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Menu;
use common\grid\EnumColumn;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Создать категорию меню', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'description:ntext',
            [
                'class' => EnumColumn::class,
                'attribute' => 'parent_id',
                'enum' => ArrayHelper::map($parents, 'id', 'name'),
                'filter' => ArrayHelper::map($parents, 'id', 'name'),
            ],
            [
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => Menu::statuses(),
                'filter' => Menu::statuses(),
            ],
            [
                'attribute' => 'image', 'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image)
                        return '<img src="' . $model->getImg() . '" width="240" height="auto">'; else return 'no image';
                },
            ],
            [
                'attribute' => 'banner_image', 'format' => 'raw',
                'value' => function ($model) {
                    if ($model->banner_image)
                        return '<img src="' . $model->getBanner() . '" width="240" height="auto">'; else return 'no image';
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
