<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Poster;
use common\models\OrderPoster;
use common\grid\EnumColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrderPosterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Posters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-poster-index">

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'poster_id',
                'options' => ['style' => 'width: 15%'],
                'value' => function ($model) {
                    return Html::a($model->poster->name,['/poster/view','id' => $model->poster_id],['target'=>'_blank']);
                },
                'filter' => ArrayHelper::map(Poster::find()->all(), 'id', 'name'),
                'format' => 'raw'

            ],
            'name',
            'phone',
            'email:email',
             'count',
             'totalPrice',
             'note:ntext',
            [
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => OrderPoster::statuses(),
                'filter' => OrderPoster::statuses(),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
