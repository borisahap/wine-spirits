<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OrderPoster */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Order Posters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-poster-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'poster_id',
                'value' => $model->poster->name,
            ],
            'name',
            'phone',
            'email:email',
            'count',
            'totalPrice',
            'note:ntext',
            [
                    'attribute' => 'status',
                'value' => \common\models\OrderPoster::statuses()[$model->status],
            ]
        ],
    ]) ?>

</div>
