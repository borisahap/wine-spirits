<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderPoster */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="order-poster-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'poster_id')->dropDownList(\yii\helpers\ArrayHelper::map($poster, 'id', 'name'), ['disabled' => true]) ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?php echo $form->field($model, 'email')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?php echo $form->field($model, 'phone')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?php echo $form->field($model, 'count')->input('number',['disabled'=>true]) ?>

    <?php echo $form->field($model, 'totalPrice')->textInput() ?>

    <?php echo $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'status')->dropDownList(\common\models\OrderPoster::statuses()) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
