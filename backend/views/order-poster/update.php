<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderPoster */

$this->title = 'Update Order Poster: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Order Posters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-poster-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'poster' => $poster
    ]) ?>

</div>
