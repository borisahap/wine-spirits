<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProgramPosterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Программа постера: '.$poster->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-poster-index">

    <p>
        <?php echo Html::a('Создать шаг', ['create','post_id'=>Yii::$app->request->get('post_id')], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'to_name',
            [
                'attribute' => 'image', 'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image)
                        return '<img src="' . $model->getImg() . '" width="240" height="auto">'; else return 'no image';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
