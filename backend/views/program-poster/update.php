<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProgramPoster */

$this->title = 'Update Program Poster: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Program Posters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="program-poster-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
