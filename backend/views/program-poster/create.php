<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProgramPoster */

$this->title = 'Create Program Poster';
$this->params['breadcrumbs'][] = ['label' => 'Program Posters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-poster-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
