<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'date')->input('date',['value'=>Yii::$app->formatter->asDate($model->date,'yyyy-MM-dd')]) ?>

    <?php echo $form->field($model, 'time')->input('time', ['maxlength' => true]) ?>

    <?php echo $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?php echo $form->field($model, 'count')->input('number') ?>

    <?php echo $form->field($model, 'note')->textarea(['rows' => 6, 'disabled' => true]) ?>
    <?php echo $form->field($model, 'my_note')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'status')->dropDownList(\common\models\Order::statuses()) ?>
    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
