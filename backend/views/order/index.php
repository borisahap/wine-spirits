<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Order;
use common\grid\EnumColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                    'attribute' => 'date',
                'value' => function($model){
                return Yii::$app->formatter->asDate($model->date);
                }
            ],
            'time',
            'phone',
            'name',
            'count',
            'note:ntext',
            'my_note:ntext',
            [
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => Order::statuses(),
                'filter' => Order::statuses(),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
