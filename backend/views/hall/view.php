<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Hall */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Halls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hall-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'sub_name',
            [
                'attribute' => 'description',
                'value' => strip_tags($model->description),
            ],

            [
                'attribute' => 'image',
                'value' => $model->getImg(),
                'format' => ['image', ['width' => '240', 'height' => 'auto']],

            ],
        ],
    ]) ?>

</div>
