<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Hall */

$this->title = 'Create Hall';
$this->params['breadcrumbs'][] = ['label' => 'Halls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hall-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
