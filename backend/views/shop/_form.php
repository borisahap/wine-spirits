<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Shop */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="shop-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png','jpeg'],'showUpload' => true,

            'initialPreview'=>[
                $model->image != null ? $model->getImg() : '',
            ],

            'initialPreviewAsData'=>true,
            'initialCaption'=>$model->image != null ? $model->image : '',
        ]

    ]); ?>

    <?php echo $form->field($model, 'phone')
        ->hint(Yii::t('backend', 'Если телефонов несколько, укажите через запятую c пробелом'))
        ->textInput(['maxlength' => true,'placeholder'=> '+xxx-xx-xxx-xx-xx, ...']) ?>

    <?php echo $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'time')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
            ],
        ]
    ) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
