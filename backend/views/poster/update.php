<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Poster */

$this->title = 'Редактирование мероприятия: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Мероприятия', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="poster-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
