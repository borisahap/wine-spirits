<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use trntv\filekit\widget\Upload;
/* @var $this yii\web\View */
/* @var $model common\models\Poster */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="poster-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>


    <?php echo $form->field($model, 'thumbnail')->widget(
        Upload::class,
        [
            'url' => ['/file/storage/upload'],
            'maxFileSize' => 5000000, // 5 MiB
        ]);
    ?>

    <?php echo $form->field($model, 'small_description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'description')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'date')->input('date',['maxlength' => true, 'value' => $model->date != null ? Yii::$app->formatter->asDate($model->date,'yyyy-MM-dd') : '']) ?>

    <?php echo $form->field($model, 'time')->input('time',['maxlength' => true]) ?>

    <?php echo $form->field($model, 'price')->input('number',['min'=>1, 'step'=>0.5]) ?>

    <?php echo $form->field($model, 'count_place')->input('number', ['min' => 0, 'max' => '99999', 'value' => 1, 'step' => 1]) ?>

    <?php echo $form->field($model, 'place')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'phone')->textInput(['maxlength' => true, 'value' => Yii::$app->keyStorage->get('phone1')]) ?>

    <?php echo $form->field($model, 'status')->dropDownList(\common\models\Poster::statuses()) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
