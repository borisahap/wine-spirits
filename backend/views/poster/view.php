<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Poster;

/* @var $this yii\web\View */
/* @var $model common\models\Poster */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Мероприятия', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poster-view">

    <p>
        <?php echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Программа мероприятия', ['/program-poster/index', 'post_id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?php echo Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'meta_title',
            'meta_description:ntext',
            'name',
            'small_description:ntext',
            [
                'attribute' => 'description',
                'value' => strip_tags($model->description)
            ],

            [
                'attribute' => 'date',
                'value' => Yii::$app->formatter->asDate($model->date,'dd-MM-yyyy')
            ],
            'time',
            'price',
            'place',
            'phone',
            [
                'attribute' => 'status',
                'value' => Poster::statuses()[$model->status]
            ],
            [
                'attribute' => 'thumbnail',
                'value'=>$model->getImg(),
                'format' => ['image',['width'=>'240','height'=>'auto']],

            ],
        ],
    ]) ?>

</div>
