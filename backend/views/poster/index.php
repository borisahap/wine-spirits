<?php

use common\grid\EnumColumn;
use trntv\yii\datetime\DateTimeWidget;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PosterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мероприятия';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poster-index">

    <p>
        <?php echo Html::a('Создать мероприятия', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'date',
                'options' => ['style' => 'width: 10%'],
                'format' => 'date',
                'filter' => DateTimeWidget::widget([
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientEvents' => [
                        'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                    ],
                ]),
            ],
             'time',
             'price',
             'place',
             'phone',
            [
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => \common\models\Poster::statuses(),
                'filter' => \common\models\Poster::statuses(),
            ],
            [
                'attribute' => 'thumbnail', 'format' => 'raw',
                'value' => function ($model) {
                    if ($model->thumbnail_base_url && $model->thumbnail_path)
                        return '<img src="' . $model->getImg() . '" width="240" height="auto">'; else return 'no image';
                },
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
