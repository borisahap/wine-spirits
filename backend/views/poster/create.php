<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Poster */

$this->title = 'Создание мероприятия';
$this->params['breadcrumbs'][] = ['label' => 'Мироприятия', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poster-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
