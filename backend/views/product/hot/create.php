<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Hot */

$this->title = 'Создание спецпредложения';
$this->params['breadcrumbs'][] = ['label' => 'Hots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hot-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'cats' => $cats
    ]) ?>

</div>
