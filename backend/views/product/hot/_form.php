<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Hot;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Hot */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="hot-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'cat_id')->dropDownList(ArrayHelper::map($cats,'id','name')) ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png','jpeg'],'showUpload' => true,

            'initialPreview'=>[
                $model->image != null ? $model->getImg() : '',
            ],

            'initialPreviewAsData'=>true,
            'initialCaption'=>$model->image != null ? $model->image : '',
        ]

    ]); ?>

    <?php echo $form->field($model, 'status')->dropDownList(Hot::statuses()) ?>

    <?php echo $form->field($model, 'date_start')->input('date',['maxlength' => true,'value' => $model->date_start != null ? Yii::$app->formatter->asDate($model->date_start,'yyyy-MM-dd') : '']) ?>

    <?php echo $form->field($model, 'date_finished')->input('date',['maxlength' => true, 'value' => $model->date_finished != null ? Yii::$app->formatter->asDate($model->date_finished,'yyyy-MM-dd') : '']) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
