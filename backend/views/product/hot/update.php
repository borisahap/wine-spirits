<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Hot */

$this->title = 'Редактирование спецпредложения' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Спецпредложения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] =  'Редактирование';
?>
<div class="hot-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'cats' => $cats
    ]) ?>

</div>
