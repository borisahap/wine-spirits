<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use yii\helpers\ArrayHelper;
use common\models\Hot;
use trntv\yii\datetime\DateTimeWidget;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\HotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Спецпредложения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hot-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Создать спецпредложение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => EnumColumn::class,
                'attribute' => 'cat_id',
                'enum' => ArrayHelper::map($cats, 'id', 'name'),
                'filter' => ArrayHelper::map($cats, 'id', 'name'),
            ],
            'name',
            [
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => Hot::statuses(),
                'filter' => Hot::statuses(),
            ],
            [
                'attribute' => 'date_start',
                'options' => ['style' => 'width: 10%'],
                'format' => 'date',
                'filter' => DateTimeWidget::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_start',
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientEvents' => [
                        'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                    ],
                ]),
            ],
            [
                'attribute' => 'date_finished',
                'options' => ['style' => 'width: 10%'],
                'format' => 'date',
                'filter' => DateTimeWidget::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_finished',
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientEvents' => [
                        'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                    ],
                ]),
            ],
            [
                'attribute' => 'image', 'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image)
                        return '<img src="' . $model->getImg() . '" width="240" height="auto">'; else return 'no image';
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
