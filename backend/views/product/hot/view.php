<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Hot;

/* @var $this yii\web\View */
/* @var $model common\models\Hot */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' =>  'Спецпредложения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hot-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('backend', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' =>'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'cat_id',
                'value' => $model->category->name
            ],
            'name',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => Hot::statuses()[$model->status]
            ],
            [
                    'attribute' => 'date_start',
                'value' => Yii::$app->formatter->asDate($model->date_start,'dd-MM-yyyy')
            ],
            [
                'attribute' => 'date_finished',
                'value' => Yii::$app->formatter->asDate($model->date_finished,'dd-MM-yyyy')
            ],
            [
                'attribute' => 'image',
                'value'=>$model->getImg(),
                'format' => ['image',['width'=>'240','height'=>'auto']],

            ],
        ],
    ]) ?>

</div>
