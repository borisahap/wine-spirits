<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'cat_id')->dropDownList(ArrayHelper::map($cats,'id','name')) ?>

    <?php echo $form->field($model, 'name')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'price')->input('number',['step'=>0.1,'min'=>0]) ?>

    <?php echo $form->field($model, 'status')->dropDownList(\common\models\Product::statuses()) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
