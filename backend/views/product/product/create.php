<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = 'Добавление продукции';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Продукция'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'cats' => $cats
    ]) ?>

</div>
