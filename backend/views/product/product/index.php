<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Product;
use common\grid\EnumColumn;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукция';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <p>
        <?php echo Html::a('Добавить продукцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => EnumColumn::class,
                'attribute' => 'cat_id',
                'enum' => ArrayHelper::map($cats, 'id', 'name'),
                'filter' => ArrayHelper::map($cats, 'id', 'name'),
            ],
            'name:ntext',
            'price',
            [
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => Product::statuses(),
                'filter' => Product::statuses(),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
