<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\keyStorage\FormWidget;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php echo FormWidget::widget([
    'model' => $model,
    'formClass' => '\yii\bootstrap\ActiveForm',
    'submitText' => Yii::t('backend', 'save'),
    'submitOptions' => ['class' => 'btn btn-primary'],
]) ?>
<br>
<div class="service-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Service', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            ['attribute' => 'description',
                'value' => function ($model) {
                    return strip_tags($model->description);
                }],
            [
                'attribute' => 'image', 'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image)
                        return '<img src="' . $model->getImg() . '" width="240" height="auto">'; else return 'no image';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
