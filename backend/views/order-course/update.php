<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderCourse */

$this->title = 'Update Order Course: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Order Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-course-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'course' => $course
    ]) ?>

</div>
