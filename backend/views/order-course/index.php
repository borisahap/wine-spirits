<?php

use common\grid\EnumColumn;
use common\models\OrderCourse;
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Courses;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrderCourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Courses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-course-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'course_id',
                'options' => ['style' => 'width: 15%'],
                'value' => function ($model) {
                    return Html::a($model->course->name,['/course/courses/view','id' => $model->course_id],['target'=>'_blank']);
                },
                'filter' => ArrayHelper::map(Courses::find()->all(), 'id', 'name'),
                'format' => 'raw'

            ],
            'name',
            'email:email',
            'phone',
             'count',
            'note',
            [
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => OrderCourse::statuses(),
                'filter' => OrderCourse::statuses(),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
