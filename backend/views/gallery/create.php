<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Gallery */

$this->title = Yii::t('backend', 'Новая галерея');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Галлерея'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
