<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Галлерея');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Создать новую галерею'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                    'attribute' => 'name',

                'options' => ['style' => 'width: 23%'],

            ],
            [
                'attribute' => 'image', 'format' => 'raw',
                'value' => function ($model) {
                    $string = '';
                    foreach ($model->imgGalleries as $image) {
                        $string = $string . ' ' . '<img src="' . $image->getImg() . '" width="120" height="auto">';
                    }
                    return trim($string);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'width: 6%'],
                'template' => '{delete} {update}',
            ],
        ],
    ]); ?>

</div>
