<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Команда');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">
    <p>
        <?php echo Html::a(Yii::t('backend', 'Добавить в команду'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            ['attribute' => 'description',
                'value' => function ($model) {
                    return strip_tags($model->description);
                }],
            'position',
            [
                'attribute' => 'image', 'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image)
                        return '<img src="' . $model->getImg() . '" width="240" height="auto">'; else return 'no image';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
