<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GeneralInfoCourses */

$this->title = 'Create General Info Courses';
$this->params['breadcrumbs'][] = ['label' => 'General Info Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-info-courses-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
