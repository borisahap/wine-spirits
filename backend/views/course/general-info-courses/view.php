<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\GeneralInfoCourses */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'General Info Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-info-courses-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description:ntext',
            [
                'attribute' => 'image',
                'value'=>$model->getImg(),
                'format' => ['image',['width'=>'240','height'=>'auto']],

            ],
        ],
    ]) ?>

</div>
