<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProgramCourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Program Courses: '.$course->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-course-index">

    <p>
        <?php echo Html::a('Create Program Course', ['create','course_id'=>Yii::$app->request->get('course_id')], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'description:html',
            'date:date',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
