<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProgramCourse */

$this->title = 'Create Program Course';
$this->params['breadcrumbs'][] = ['label' => 'Program Courses', 'url' => ['index' , 'course_id'=>Yii::$app->request->get('course_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-course-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
