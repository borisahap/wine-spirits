<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProgramCourse */

$this->title = 'Update Program Course: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Program Courses', 'url' => ['index','course_id'=> $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="program-course-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
