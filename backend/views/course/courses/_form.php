<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Courses */
/* @var $form yii\bootstrap\ActiveForm */
?>
<?php if (count($teachers) == 0) : ?>
<div class="course-form">
    Пожалуйста, <?= Html::a('добавьте сначала преподавателей',Url::toRoute(['/course/teachers/index'])) ?>
</div>
<?php else :?>
<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png','jpeg'],'showUpload' => true,

            'initialPreview'=>[
                $model->image != null ? $model->getImg() : '',
            ],

            'initialPreviewAsData'=>true,
            'initialCaption'=>$model->image != null ? $model->image : '',
        ]

    ]); ?>

    <?php echo $form->field($model, 'description')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'result')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'after_course')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'trainig')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'exam')->checkbox() ?>

    <?php echo $form->field($model, 'price')->input('number',['min'=>0,'step'=>0.1,'value'=>0]) ?>

    <?php echo $form->field($model, 'count')->input('number',['min'=>0,'step'=>0.1,'value'=>0]) ?>

    <?php echo $form->field($model, 'place')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'phone')->textInput(['maxlength' => true, 'value'=> Yii::$app->keyStorage->get('phone1')]) ?>

    <?php echo $form->field($model, 'date')->input('date',['maxlength' => true ,'value' => $model->date != null ? Yii::$app->formatter->asDate($model->date,'yyyy-MM-dd') : '']) ?>

    <?php echo $form->field($model, 'time')->textInput(['maxlength' => true,'placegolder' => 'Вторник, с 12.00 до 15.00']) ?>

    <?php echo $form->field($model, 'note')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'status')->dropDownList(\common\models\Courses::statuses()) ?>

    <?php echo $form->field($model, 'teacher_id')->dropDownList(\yii\helpers\ArrayHelper::map($teachers,'id','name')) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php endif; ?>