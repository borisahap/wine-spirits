<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Courses */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Программа курса', ['/course/program-courses/index', 'course_id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'meta_title',
            'meta_description:ntext',
            'name',
            [
                'attribute' => 'description',
                'value' => $model->description,
                'format' => ['html']
            ],
            [
                'attribute' => 'result',
                'value' =>$model->result,
                'format' => ['html']
            ],
            [
                'attribute' => 'after_course',
                'value' => $model->after_course,
                'format' => ['html']
            ],
            [
                'attribute' => 'trainig',
                'value' => $model->after_course,
                'format' => ['html']
            ],
            'exam',
            'price',
            'count',
            'place',
            'phone',
            [
                'attribute' => 'date',
                'value' => Yii::$app->formatter->asDate($model->date,'dd.MM.yyyy'),
            ],
            'time',
            [
                'attribute' => 'note',
                'value' => $model->note,
                'format' => ['html']
            ],
            [
                'attribute' => 'status',
                'value'=> \common\models\Courses::statuses()[$model->status],

            ],
            [
                'attribute' => 'teacher_id',
                'value'=> $model->teacher->getImg(),
                'format' => ['image',['title'=>$model->teacher->name, 'alt'=>$model->teacher->name,'width'=>'240','height'=>'auto']],
            ],
            [
                'attribute' => 'image',
                'value'=>$model->getImg(),
                'format' => ['image',['width'=>'240','height'=>'auto']],

            ],
        ],
    ]) ?>

</div>
