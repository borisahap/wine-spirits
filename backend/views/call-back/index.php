<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use common\models\CallBack;
use trntv\yii\datetime\DateTimeWidget;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CallBackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Call Backs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-back-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'note:ntext',
            'phone',
            [
                'attribute' => 'created_at',
                'options' => ['style' => 'width: 10%'],
                'format' => 'datetime',
                'filter' => DateTimeWidget::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientEvents' => [
                        'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                    ],
                ]),
            ],
            [
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => CallBack::statuses(),
                'filter' => CallBack::statuses(),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
