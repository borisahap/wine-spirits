<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CallBack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="call-back-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true,'disabled'=>true]) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6,'disabled'=>true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'disabled'=>true]) ?>


    <?= $form->field($model, 'status')->dropDownList(\common\models\CallBack::statuses()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
